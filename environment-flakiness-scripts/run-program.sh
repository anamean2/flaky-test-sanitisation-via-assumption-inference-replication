#!/bin/bash
PROGRAM=$1
SAFLATE=$2
JDK=$3
TOTAL_RUNS=$4

if [ -d "./environment-flakiness-scripts" ] 
then
    echo "Checked working directory" 
else
    echo "Current working directory must be package root"
    exit 1;
fi


if [ "$4" -ge 1 ]; then
 echo "Total runs: $4";
else
 echo "Invalid number of runs: $4";
 exit -1;
fi 

cd environment-flakiness-scripts

if test -f "docker/env-$JDK.list"; then
  echo "jdk: env-$JDK.list"
else
  echo "jdk not valid!"
  exit -1
fi
P=""
if [ "$SAFLATE" == "saflateon" ]; then
  P="$PROGRAM-saflated.zip"
  if test -f "../dataset/environment/$P"; then
    echo "Program: $P"
  else
    echo "Program not in dataset: ../dataset/environment/$P"
    exit -1; 
  fi
fi
if [ "$SAFLATE" == "saflateoff" ]; then
  P="$PROGRAM.zip"
  if test -f "../dataset/environment/$P"; then
    echo "Program: $P"
  else
    echo "Program not in dataset: ../dataset/environment/$P"
    exit -1
  fi
fi
if [ "$P" == "" ]; then
  echo "program: $PROGRAM not in dataset"
  exit -1
fi

mkdir docker/mnt/cache
mkdir docker/mnt/out
mkdir docker/mnt/data
rm -rf docker/mnt/data/*
rm docker/env.list
cat docker/env-$JDK.list | tee docker/env.list
echo "TOTAL_RUNS=$TOTAL_RUNS" | tee -a docker/env.list
echo "JDK=$JDK" | tee -a docker/env.list
echo "SAFLATE=$SAFLATE" | tee -a docker/env.list
echo "PROG=$PROGRAM" | tee -a docker/env.list
cp ../dataset/environment/$P docker/mnt/data
cd docker/mnt/data
unzip $P
cd ../../../
cd docker
./run-docker.sh
cd ..

echo "$1 $2 $3"
