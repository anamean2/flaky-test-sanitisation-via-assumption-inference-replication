#!/bin/bash
cd /mnt/data
cd $PROG
for i in $(seq 1 $TOTAL_RUNS); do
 mvn -Dmaven.repo.local=../../cache clean test -fn -Dsaflate.environ.classifier=$PWD/classifier.weka -Dsaflate.environ.arff=$PWD/data.arff
 TGT=/mnt/out/testresults-environ-$PROG-linux$JDK-$SAFLATE-$i
 mkdir $TGT
 find . -type f -regex '.*TEST.*.xml'  -exec cp "{}" $TGT \;
done
mvn clean
