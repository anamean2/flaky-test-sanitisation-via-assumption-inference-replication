#!/bin/bash
if [ -d "./environment-flakiness-scripts" ]
then
    echo "Checked working directory"
else
    echo "Current working directory must be package root"
    exit 1;
fi

cd saflate-environ-core
mvn clean test > /dev/null 2>&1
mvn dependency:copy-dependencies > /dev/null 2>&1
cd ..
echo "Extracting training data archive"
cp results/environment/results.zip environment-flakiness-scripts/scripts/
cd environment-flakiness-scripts/scripts
unzip results.zip
cd ../..
echo "Installing dependencies in local maven repo"
cd saflate-environ-core
mvn install -Dmaven.repo.local=../environment-flakiness-scripts/docker/mnt/cache > /dev/null 2>&1
cd ../saflate-environ-junit4
mvn install -Dmaven.repo.local=../environment-flakiness-scripts/docker/mnt/cache > /dev/null 2>&1
cd ..

