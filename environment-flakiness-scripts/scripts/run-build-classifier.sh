#!/bin/bash
if [ -d "./environment-flakiness-scripts" ]
then
    echo "Checked working directory"
else
    echo "Current working directory must be package root"
    exit 1;
fi

BASE_DIR=$PWD
cd environment-flakiness-scripts/scripts
mkdir out
PROJECT=$1
DIRS=`find $PWD/training -type d | grep $PROJECT | grep saflate | tr \\\n , | sed 's/.$//'`
java -Xmx32g -Dlog4j.configurationFile=$PWD/log4j2.xml -cp $BASE_DIR/saflate-environ-core/target/dependency/*:$BASE_DIR/saflate-environ-core/target/classes nz.ac.wgtn.ecs.saflate.environ.train.Main -arff out/$PROJECT.arff -classifier out/$PROJECT.weka -surefirereports $DIRS -evaluation out/$PROJECT-evaluation.txt -folds 10 -provenance out/$PROJECT-rules.txt

cd -
