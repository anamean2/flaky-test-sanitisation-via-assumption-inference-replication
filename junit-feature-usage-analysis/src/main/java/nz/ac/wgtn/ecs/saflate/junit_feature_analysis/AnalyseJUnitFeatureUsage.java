package nz.ac.wgtn.ecs.saflate.junit_feature_analysis;

import com.google.common.base.Preconditions;

import org.apache.commons.text.*;
import org.objectweb.asm.*;
import java.io.*;
import java.text.NumberFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Script to analyse the use of junit4/5 @Test annotations, disabled/ignore annotations, and built-in assertions and assumptions.
 */
public class AnalyseJUnitFeatureUsage {

    public static final String OUTPUT_DETAILED = "junit-feature-usage-detailed.tex";
    public static final String OUTPUT_SUMMARISED = "junit-feature-usage-summarised.tex";
    public static final String DATA_ROOT_FOLDER = "data";

    public static final String CLASSES = "class-count";
    public static final String TESTS_5 = "@Test#5";
    public static final String ASSERTS_5= "assert#5";
    public static final String ASSUMES_5 = "assume#5";
    public static final String DISABLED_5 = "@Disabled#5";
    public static final String TESTS_4 = "@Test#4";
    public static final String ASSERTS_4 = "assert#4";
    public static final String ASSUMES_4 = "assume#4";
    public static final String IGNORE_4 = "@Ignore#5";

    public static final String IGNORED = "@Ignore";
    public static final String TESTS = "@Test";
    public static final String ASSERTS = "asserts";
    public static final String ASSUMES = "assumes";

    public static final String CONDITIONS = "conditions";

    public static final String[] KEYS_DETAILED = {CLASSES,TESTS_5,TESTS_4,ASSERTS_5,ASSERTS_4,ASSUMES_5,ASSUMES_4,DISABLED_5,IGNORE_4,CONDITIONS};
    public static final String[] KEYS_SUMMARY = {CLASSES,TESTS,ASSERTS,ASSUMES,IGNORED,CONDITIONS};

    public static void main (String[] args) throws IOException {
        File root = new File(DATA_ROOT_FOLDER);
        Preconditions.checkState(root.exists());
        List<File> programs = Stream.of(root.listFiles())
            .filter(f -> !f.isHidden())
            .filter(f -> !f.isDirectory())
            .filter(f -> f.getName().endsWith(".zip"))
            .collect(Collectors.toList());

        Map<String,Map<String,Integer>> allCounts = new TreeMap<>();
        for (File program:programs) {
            String name = program.getName().replace(".zip","");
            System.out.println("analysing: " + name);

            Map<String,Integer> counts = new HashMap<>();
            allCounts.put(name,counts);

            ZipFile zip = new ZipFile(program);
            Enumeration<? extends ZipEntry> en = zip.entries();
            Set<String> classesWithTests = new HashSet<>();
            while (en.hasMoreElements()) {
                ZipEntry e = en.nextElement();
                if (e.getName().endsWith(".class")) {
                    increaseCount(counts,CLASSES);
                    InputStream in = zip.getInputStream(e);
                    new ClassReader(in).accept(new FeatureCounter(counts,classesWithTests), 0);
                }
            }
            counts.put(CLASSES,classesWithTests.size());

            // note -- this is not just logging but will also initialise keys to 0 if necessary
            System.out.println("Detailed stats for " + program.getName());
            for (String key: KEYS_DETAILED) {
                System.out.println("\t" + key + " -> " + counts.computeIfAbsent(key,k -> 0));
            }

            System.out.println("Summarised stats for " + program.getName());
            for (String key: KEYS_SUMMARY) {
                System.out.println("\t" + key + " -> " + counts.computeIfAbsent(key,k -> 0));
            }
        }

        exportResultsDetails(allCounts);
        System.out.println("results written to " + new File(OUTPUT_DETAILED).getAbsolutePath());

        exportResultsSummarised(allCounts);
        System.out.println("results written to " + new File(OUTPUT_SUMMARISED).getAbsolutePath());
    }

    // details -- report junit4 / junit5 separately
    private static void exportResultsDetails(Map<String,Map<String,Integer>> allCounts) {

        try (PrintWriter out = new PrintWriter(new FileWriter(OUTPUT_DETAILED))) {
            out.println("\\begin{table*}[]");
            out.println("\\begin{tabular}{|c|c|cc|cc|cc|cc|c|}");
            out.println("\\hline");
            out.println("\\multirow{2}{*}{program} & \\multirow{2}{*}{classes} & \\multicolumn{2}{c|}{@Test methods}  & \\multicolumn{2}{c|}{assertions} & \\multicolumn{2}{c|}{assumptions} & \\multicolumn{2}{c|}{@Disabled/@Ignore} & conditions \\\\");
            out.println(" & & junit4   &  junit5  & junit4 & junit5  & junit4 &  junit5 & junit4 &  junit5 & \\\\ \\hline");


            for (String program:allCounts.keySet()) {
                Map<String,Integer> counts = allCounts.get(program);
                out.print(WordUtils.capitalize(program));
                out.print(" & ");
                out.print(format(counts.get(CLASSES)));
                out.print(" & ");
                out.print(format(counts.get(TESTS_4)));
                out.print(" & ");
                out.print(format(counts.get(TESTS_5)));
                out.print(" & ");
                out.print(format(counts.get(ASSERTS_4)));
                out.print(" & ");
                out.print(format(counts.get(ASSERTS_5)));
                out.print(" & ");
                out.print(format(counts.get(ASSUMES_4)));
                out.print(" & ");
                out.print(format(counts.get(ASSUMES_5)));
                out.print(" & ");
                out.print(format(counts.get(IGNORE_4)));
                out.print(" & ");
                out.print(format(counts.get(DISABLED_5)));
                out.print(" & ");
                out.print(format(counts.get(IGNORED)));
                out.print(" & ");
                out.print(format(counts.get(CONDITIONS)));
                out.print(" \\\\");
                out.println();
            }
            out.print("\\hline");
            out.println("\\end{tabular}");
            out.println("\\caption{Use of JUnit features in programs}");
            out.println("\\label{tab:junit-feature-use}");
            out.println("\\end{table*}");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // details -- combine junit4 & junit5
    private static void exportResultsSummarised(Map<String,Map<String,Integer>> allCounts) {

        try (PrintWriter out = new PrintWriter(new FileWriter(OUTPUT_SUMMARISED))) {
            out.println("\\begin{table}[]");
            out.println("\\begin{tabular}{|lrrrrrr|}");
            out.println("\\hline");
            out.println(" program  & classes   &  tests  & assrt. & assum.  & ign./dis. & cond. \\\\ \\hline");

            for (String program:allCounts.keySet()) {
                Map<String,Integer> counts = allCounts.get(program);
                out.print(program); //WordUtils.capitalize(program)
                out.print(" & ");
                out.print(format(counts.get(CLASSES)));
                out.print(" & ");
                out.print(format(counts.get(TESTS)));
                out.print(" & ");
                out.print(format(counts.get(ASSERTS)));
                out.print(" & ");
                out.print(format(counts.get(ASSUMES)));
                out.print(" & ");
                out.print(format(counts.get(IGNORED)));
                out.print(" & ");
                out.print(format(counts.get(CONDITIONS)));
                out.print(" \\\\");
                out.println();
            }
            out.print("\\hline");
            out.println("\\end{tabular}");
            out.println("\\caption{Use of JUnit features in programs}");
            out.println("\\label{tab:junit-feature-use}");
            out.println("\\end{table}");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private static String format(int c) {
        return NumberFormat.getNumberInstance(Locale.US).format(c);
    }

    static void increaseCount(Map<String,Integer> counts,String key) {
        counts.compute(key,(k,v) -> v==null?1:(1+v));
    }

    static class FeatureCounter extends ClassVisitor {
        Map<String,Integer> counts = null;
        Set<String> classesWithTests = null;
        String currentClass = null;
        public FeatureCounter(Map<String, Integer> counts,Set<String> classesWithTests) {
            super(Opcodes.ASM9);
            this.counts = counts;
            this.classesWithTests = classesWithTests;
        }

        @Override
        public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
            super.visit(version, access, name, signature, superName, interfaces);
            this.currentClass = name;
        }

        @Override
        public AnnotationVisitor visitAnnotation(String descriptor, boolean visible) {
            if (Objects.equals("Lorg/junit/jupiter/api/Test;",descriptor)) {
                increaseCount(FeatureCounter.this.counts,TESTS_5);
                increaseCount(FeatureCounter.this.counts,TESTS);
                classesWithTests.add(currentClass);
            }
            else if (Objects.equals("Lorg/junit/jupiter/params/ParameterizedTest;",descriptor)) {
                increaseCount(FeatureCounter.this.counts,TESTS_5);
                increaseCount(FeatureCounter.this.counts,TESTS);
                classesWithTests.add(currentClass);
            }
            else if (Objects.equals("Lorg/junit/Test;",descriptor)) {
                increaseCount(FeatureCounter.this.counts,TESTS_4);
                increaseCount(FeatureCounter.this.counts,TESTS);
                classesWithTests.add(currentClass);
            }
            else if (Objects.equals("Lorg/junit/jupiter/api/Disabled;",descriptor)) {
                increaseCount(FeatureCounter.this.counts,DISABLED_5);
                increaseCount(FeatureCounter.this.counts,IGNORED);
            }
            else if (Objects.equals("Lorg/junit/Ignore;",descriptor)) {
                increaseCount(FeatureCounter.this.counts,IGNORE_4);
                increaseCount(FeatureCounter.this.counts,IGNORED);
            }
            else if (descriptor.startsWith("Lorg/junit/jupiter/api/condition/")) {
                increaseCount(FeatureCounter.this.counts,CONDITIONS);
            }
            return super.visitAnnotation(descriptor, visible);
        }

        @Override
        public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions) {
            return new MethodVisitor(Opcodes.ASM9) {

                @Override
                public void visitMethodInsn(int opcode, String owner, String name, String descriptor, boolean isInterface) {
                    if (name.startsWith("assert") && Objects.equals("org/junit/jupiter/api/Assertions",owner)) {
                        increaseCount(FeatureCounter.this.counts,ASSERTS_5);
                        increaseCount(FeatureCounter.this.counts,ASSERTS);
                    }
                    else if (name.startsWith("assert") && Objects.equals("org/junit/Assert",owner)) {
                        increaseCount(FeatureCounter.this.counts,ASSERTS_4);
                        increaseCount(FeatureCounter.this.counts,ASSERTS);
                    }
                    else if (name.startsWith("assume") && Objects.equals("org/junit/jupiter/api/Assumptions",owner)) {
                        increaseCount(FeatureCounter.this.counts,ASSUMES_5);
                        increaseCount(FeatureCounter.this.counts,ASSUMES);
                    }
                    else if (name.startsWith("assume") && Objects.equals("org/junit/Assume",owner)) {
                        increaseCount(FeatureCounter.this.counts,ASSUMES_4);
                        increaseCount(FeatureCounter.this.counts,ASSUMES);
                    }
                    super.visitMethodInsn(opcode, owner, name, descriptor, isInterface);
                }

                @Override
                public AnnotationVisitor visitAnnotation(String descriptor, boolean visible) {
                    if (Objects.equals("Lorg/junit/jupiter/api/Test;",descriptor)) {
                        increaseCount(FeatureCounter.this.counts,TESTS_5);
                        increaseCount(FeatureCounter.this.counts,TESTS);
                        classesWithTests.add(currentClass);
                    }
                    else if (Objects.equals("Lorg/junit/jupiter/params/ParameterizedTest;",descriptor)) {
                        increaseCount(FeatureCounter.this.counts,TESTS_5);
                        increaseCount(FeatureCounter.this.counts,TESTS);
                        classesWithTests.add(currentClass);
                    }
                    else if (Objects.equals("Lorg/junit/Test;",descriptor)) {
                        increaseCount(FeatureCounter.this.counts,TESTS_4);
                        increaseCount(FeatureCounter.this.counts,TESTS);
                        classesWithTests.add(currentClass);
                    }
                    else if (Objects.equals("Lorg/junit/jupiter/api/Disabled;",descriptor)) {
                        increaseCount(FeatureCounter.this.counts,DISABLED_5);
                        increaseCount(FeatureCounter.this.counts,IGNORED);
                    }
                    else if (Objects.equals("Lorg/junit/Ignore;",descriptor)) {
                        increaseCount(FeatureCounter.this.counts,IGNORE_4);
                        increaseCount(FeatureCounter.this.counts,IGNORED);
                    }
                    else if (descriptor.startsWith("Lorg/junit/jupiter/api/condition/")) {
                        increaseCount(FeatureCounter.this.counts,CONDITIONS);
                    }
                    return super.visitAnnotation(descriptor, visible);
                }
            };

        }
    }

}
