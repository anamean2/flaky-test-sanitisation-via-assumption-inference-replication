The package analyses


Run `AnalyseJUnitFeatureUsage` to create latex files `junit-feature-usage-detailed.tex` and `junit-feature-usage-summarised.tex` which
contain the analysis results of the junit features used in each project. The `-detailed` file reports junit4/junit5 features separately, whereas the  `-summarised` file combines them.

To add a program to be analysed, build the project first with a target that at least compiles the tests, zip all test binaries and add the zip file to `data/`.
For instance, if the project uses `mvn`, run `mvn test` and then zip the content of the
`target/test-classes` folder. For projects with multiple modules, all `target/test-classes` folders containing `.class` files must be added to the archive.
