## README

### Prerequisites

All experiments are built inside docker containers. Docker must be installed and running before building any image. Download and install docker from here: https://docs.docker.com/get-docker/

Each experiment is built in a seperate docker image.

### Project structure
This repository contains several modular saflate  packages (each contains a seperate README file):


+ **saflate-environ-junit4**: junit4 rules to sanitise tests that are flaky due to environment dependencies.

+ **saflate-environ-junit5**: junit5 rules to sanitise tests that are flaky due to environment dependencies

+ **saflate-network-junit4**: junit4 rules to sanitise tests that are flaky due to network issues.

+ **saflate-network-junit5**: junit5 rules to sanitise tests that are flaky due to network issues.

+ **saflate-analysis**: scripts to parse experiment results (folders or zip files containing surefilre reports), and output a summary of results including runtimes and precision/false positive and recall/false negative analysis to the console.

+ **saflate-environ-core**: this module can learn dependencies of test (outcomes) on environmental settings from the surefire reports collected over various runs.

In addition, there are the following packages:

+ **junit-feature-usage-analysis**: analyse junit4 and junit5 features used in projects.

+ **environ-dataprep-eval**:

### Building docker images

To build all docker images, run `build-images.sh`

For a quick demonstration, we also provide an option to run a single image for a single experiment. To run the network experiment for PDFBox, run `build-images-network-jsoup.sh`

### Running Experiments

#### Network Experiments

Network flakiness experiment scripts are available under `~/network-flakiness-scripts/`

Run `sh` scripts using the following naming convention:

`<experiment>-<program>[-<config-details>]-(saflateon|saflateoff).sh`.

example:

`./network-jsoup-networkoff-saflateon.sh`

#### Environment Experiment

For the environment flakiness experiments, first you need to pull the docker image :

`docker pull joebloggz/jdks`

##### Running experiments

The scripts are available under `./environment-flakiness-scripts`

First setup dependencies by executing `./environment-flakiness-scripts/setup.sh`

To run tests for a program in the dataset for a configuration, execute `./environment-flakiness-scripts/run-program.sh <PROGRAM> <saflateon|saflateoff> <config> <totalRuns>`

Valid values for program are the program names in the dataset: `commonsio, jacksondataformatsbinary, jzy3dapi, pyramidio, openhtmltopdf`

Valid values for configuration are: `ibm8, ibm11, open8, open11, open16, oracle8, oracle11, oracle16`. Note that Windows is not available due to licensing issues.

##### Training and evaluating classifiers

A classifier can be trained by executing `./environment-flakiness-scripts/scripts/run-build-classifier.sh <PROGRAM>` where valid values for program were listed previously. The output file, `<PROGRAM>-evaluation.txt`, is written to `./environment-flakiness-scripts/scripts/out`



### Inspecting Results

The build results (test reports, buildlog) will be captured and stored in a file that uses the same naming convention as the `sh` scripts used to run the experiments but is proceeded by `testresults-`, example:

`testresults-network-jsoup-networkon-saflateoff.zip`

Results for network flakiness obtained from multiple reruns (10 reruns for each run confirmation per program) are available under `~/results/network/[project]/`


Results of the environment flakiness experiment,  are captured in the folder `./environment-flakiness-scripts/docker/mnt/out` the folders use the same naming convention as the network results.

Results for environment flakiness obtained from multiple reruns (10 for each run configuration per program) are available in the zip file `./results/environment/results.zip`

Results for evaluating the saflate environment extension from multiple reruns (10 for each configuration) are available in the zip file `./results/environment/evaluation.zip`
