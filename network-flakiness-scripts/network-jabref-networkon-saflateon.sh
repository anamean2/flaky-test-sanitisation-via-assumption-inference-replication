#!/bin/sh

OUTPUT=testresults-network-jabref-networkon-saflateon.zip

docker run --rm -v $PWD:/transfer saflate-network-jabref  /bin/bash   -c "cd jabref-saflated &&\
./gradlew test --continue -Djunit.jupiter.extensions.autodetection.enabled=true -Dsaflate.supportconcurrent-test-execution=true > build-summary.log;zip -r /transfer/$OUTPUT build/test-results/test/*.xml build/reports/test/index.html build-summary.log build-summary.log;" -v .:/transfer
