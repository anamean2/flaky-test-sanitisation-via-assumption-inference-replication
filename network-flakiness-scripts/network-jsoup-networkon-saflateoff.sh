#!/bin/sh

OUTPUT=testresults-network-jsoup-networkon-saflateoff.zip

docker run --rm -v $PWD:/transfer saflate-network-jsoup  /bin/bash   -c "mvn test surefire-report:report -l build-summary.log -f jsoup/pom.xml;zip -r /transfer/$OUTPUT jsoup/target/surefire-reports jsoup/target/site/surefire-report.html build-summary.log;" -v .:/transfer
