#!/bin/sh

OUTPUT=testresults-network-jsoup-networkon-saflateon.zip

docker run --rm -v $PWD:/transfer saflate-network-jsoup  /bin/bash   -c "mvn test surefire-report:report -Djunit.jupiter.extensions.autodetection.enabled=true -Dsaflate.supportconcurrent-test-execution=true -l build-summary.log -f jsoup-saflated/pom.xml;zip -r /transfer/$OUTPUT jsoup-saflated/target/surefire-reports jsoup-saflated/target/site/surefire-report.html build-summary.log;" -v .:/transfer
