#!/bin/sh

OUTPUT=testresults-network-jabref-networkoff-saflateoff.zip


docker run --rm --network none -v $PWD:/transfer saflate-network-jabref  /bin/bash   -c "cd jabref &&\
./gradlew --offline test --continue > build-summary.log; zip -r /transfer/$OUTPUT build/test-results/test/*.xml build/reports/test/index.html build-summary.log;" -v .:/transfer
