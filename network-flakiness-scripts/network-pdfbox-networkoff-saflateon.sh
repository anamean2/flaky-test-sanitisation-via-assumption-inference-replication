#!/bin/sh

OUTPUT=testresults-network-pdfbox-networkoff-saflateon.zip

docker run --rm --network none -v $PWD:/transfer saflate-network-pdfbox  /bin/bash  -c "mvn -o -fn clean install -l build-summary.log -f pdfbox-saflated/pom.xml; mvn test surefire-report:report -Djunit.jupiter.extensions.autodetection.enabled=true -Dsaflate.supportconcurrent-test-execution=true -e -l build-summary.log -f pdfbox-saflated/pom.xml;
zip -r /transfer/$OUTPUT pdfbox-saflated/pdfbox/target/surefire-reports pdfbox-saflated/pdfbox/target/site/surefire-report.html pdfbox-saflated/examples/target/surefire-reports pdfbox-saflated/examples/target/site/surefire-report.html pdfbox-saflated/fontbox/target/surefire-reports pdfbox-saflated/fontbox/target/site/surefire-report.html pdfbox-saflated/preflight/target/surefire-reports pdfbox-saflated/preflight/target/site/surefire-report.html pdfbox-saflated/tools/target/surefire-reports pdfbox-saflated/tools/target/site/surefire-report.html pdfbox-saflated/xmpbox/target/surefire-reports pdfbox-saflated/xmpbox/target/site/surefire-report.html build-summary.log;" -v .:/transfer
