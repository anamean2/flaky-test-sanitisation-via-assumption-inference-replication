#!/bin/sh

OUTPUT=testresults-network-biojava-networkon-saflateoff.zip

docker run --rm -v $PWD:/transfer saflate-network-biojava  /bin/bash   -c "mvn -fn clean install -f biojava/pom.xml -l build-summary.log; mvn surefire-report:report-only -Daggregate=true -f biojava/pom.xml;zip -r /transfer/$OUTPUT biojava/target/site/surefire-report.html biojava/biojava-aa-prop/target/surefire-reports biojava/biojava-alignment/target/surefire-reports biojava/biojava-core/target/surefire-reports biojava/biojava-genome/target/surefire-reports biojava/biojava-integrationtest/target/surefire-reports biojava/biojava-modfinder/target/surefire-reports biojava/biojava-ontology/target/surefire-reports biojava/biojava-protein-disorder/target/surefire-reports biojava/biojava-structure/target/surefire-reports biojava/biojava-structure-gui/target/surefire-reports biojava/biojava-ws/target/surefire-reports build-summary.log;" -v .:/transfer
