#!/bin/sh

OUTPUT=testresults-network-pdfbox-networkon-saflateoff.zip

docker run --rm -v $PWD:/transfer saflate-network-pdfbox  /bin/bash  -c "mvn -o -fn clean install -l build-summary.log -f pdfbox/pom.xml; mvn surefire-report:report -l build-summary.log -f pdfbox/pom.xml;
zip -r /transfer/$OUTPUT pdfbox/pdfbox/target/surefire-reports pdfbox/pdfbox/target/site/surefire-report.html pdfbox/examples/target/surefire-reports pdfbox/examples/target/site/surefire-report.html pdfbox/fontbox/target/surefire-reports pdfbox/fontbox/target/site/surefire-report.html pdfbox/preflight/target/surefire-reports pdfbox/preflight/target/site/surefire-report.html pdfbox/tools/target/surefire-reports pdfbox/tools/target/site/surefire-report.html pdfbox/xmpbox/target/surefire-reports pdfbox/xmpbox/target/site/surefire-report.html build-summary.log;" -v .:/transfer
