#!/bin/sh

OUTPUT=testresults-network-swagger-parser-networkoff-saflateoff.zip

docker run --rm --network none  -v $PWD:/transfer saflate-network-swagger-parser  /bin/bash   -c "mvn clean test surefire-report:report-only -l build-summary.log -f swagger-parser/modules/swagger-parser/pom.xml; zip -r /transfer/$OUTPUT swagger-parser/modules/swagger-parser/target/surefire-reports/TEST-*\.xml swagger-parser/modules/swagger-parser/target/site/surefire-report.html build-summary.log;" -v .:/transfer
