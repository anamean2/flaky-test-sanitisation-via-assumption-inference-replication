#!/bin/sh

OUTPUT=testresults-network-jabref-networkon-saflateoff.zip

docker run --rm -v $PWD:/transfer saflate-network-jabref  /bin/bash   -c "cd jabref &&\
./gradlew test --continue > build-summary.log;zip -r /transfer/$OUTPUT build/test-results/test/*.xml build/reports/test/index.html build-summary.log;" -v .:/transfer
