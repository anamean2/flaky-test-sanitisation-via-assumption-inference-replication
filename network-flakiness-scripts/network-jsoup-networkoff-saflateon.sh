#!/bin/sh

OUTPUT=testresults-network-jsoup-networkoff-saflateon.zip


docker run --rm --network none -v $PWD:/transfer saflate-network-jsoup /bin/bash -c "mvn -o -fn test surefire-report:report -Djunit.jupiter.extensions.autodetection.enabled=true -Dsaflate.supportconcurrent-test-execution=true -l build-summary.log -f jsoup-saflated/pom.xml;zip -r /transfer/$OUTPUT jsoup-saflated/target/surefire-reports jsoup-saflated/target/site/surefire-report.html build-summary.log;" -v .:/transfer
