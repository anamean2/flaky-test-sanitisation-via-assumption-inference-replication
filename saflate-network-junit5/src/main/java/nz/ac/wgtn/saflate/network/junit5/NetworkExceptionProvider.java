package nz.ac.wgtn.saflate.network.junit5;

/**
 * Service to define which exceptions should be sanitised.
 */
public interface NetworkExceptionProvider {
    Class<? extends Exception>[] getNetworkExceptionClassesToBeSanitised();
}
