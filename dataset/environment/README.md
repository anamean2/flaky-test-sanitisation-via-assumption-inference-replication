## README
### List of projects used

| program                   | tag                                   | URL                                                     |
|---------------------------|---------------------------------------|---------------------------------------------------------|
| openhtmltopdf             | openhtmltopdf-parent-1.0.9            | https://github.com/danfickle/openhtmltopdf              |
| commons-io                | rel/commons-io-2.11.0                 | https://github.com/apache/commons-io                    |
| pyramid-io                | v1.1.0                                | https://github.com/usnistgov/pyramidio                  |
| jackson-dataformat-binary | jackson-dataformats-binary-2.13.0-rc2 | https://github.com/FasterXML/jackson-dataformats-binary |
| jzy3d-api                 | jzy3d-all-2.0.0                       | https://github.com/jzy3d/jzy3d-api                      |
