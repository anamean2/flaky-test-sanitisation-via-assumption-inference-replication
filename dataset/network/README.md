## README

### List of projects used

| Program        | tag                  | URL                                           |
|----------------|----------------------|-----------------------------------------------|
| jabref         | v5.0                 | https://github.com/JabRef/jabref              |
| biojava        | biojava-6.0.0-alpha4 | https://github.com/biojava/biojava            |
| pdfbox         | 2.0.24               | https://github.com/apache/pdfbox              |
| jsoup          | jsoup-1.14.2         | https://github.com/jhy/jsoup                  |
| swagger-parser | v2.0.18              | https://github.com/swagger-api/swagger-parser |
