#!/bin/sh

counter=1

until [ $counter -gt 10 ]
do

  output=testresults-network-jsoup-networkon-saflateoff-${counter}.zip

  # cd networkon-saflateoff
  mkdir -p "networkon-saflateoff"  && cd "$_"

  docker run --rm -v $PWD:/transfer saflate-network-jsoup  /bin/bash   -c "mvn test surefire-report:report -l build-summary.log -f jsoup/pom.xml;zip -r /transfer/$output jsoup/target/surefire-reports jsoup/target/site/surefire-report.html build-summary.log;" -v .:/transfer

  cd ..

  output=testresults-network-jsoup-networkon-saflateon-${counter}.zip

  # cd networkon-saflateon
  mkdir -p "networkon-saflateon"  && cd "$_"

  docker run --rm -v $PWD:/transfer saflate-network-jsoup  /bin/bash   -c "mvn test surefire-report:report -Djunit.jupiter.extensions.autodetection.enabled=true -Dsaflate.supportconcurrent-test-execution=true -l build-summary.log -f jsoup-saflated/pom.xml;zip -r /transfer/$output jsoup-saflated/target/surefire-reports jsoup-saflated/target/site/surefire-report.html build-summary.log;" -v .:/transfer

  cd ..

  output=testresults-network-jsoup-networkoff-saflateon-${counter}.zip

  # cd networkoff-saflateon
  mkdir -p "networkoff-saflateon"  && cd "$_"

  docker run --rm --network none -v $PWD:/transfer saflate-network-jsoup /bin/bash -c "mvn -o -fn test surefire-report:report -Djunit.jupiter.extensions.autodetection.enabled=true -Dsaflate.supportconcurrent-test-execution=true -l build-summary.log -f jsoup-saflated/pom.xml;zip -r /transfer/$output jsoup-saflated/target/surefire-reports jsoup-saflated/target/site/surefire-report.html build-summary.log;" -v .:/transfer

  cd ..

  output=testresults-network-jsoup-networkoff-saflateoff-${counter}.zip

  # cd networkoff-saflateoff
  mkdir -p "networkoff-saflateoff"  && cd "$_"

  docker run --rm --network none -v $PWD:/transfer saflate-network-jsoup /bin/bash -c "mvn -o -fn surefire-report:report -l build-summary.log -f jsoup/pom.xml;zip -r /transfer/$output jsoup/target/surefire-reports jsoup/target/site/surefire-report.html build-summary.log;" -v .:/transfer

  cd ..



  ((counter++))

done

echo All done
