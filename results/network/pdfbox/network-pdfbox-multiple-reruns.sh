#!/bin/sh

counter=1

until [ $counter -gt 10 ]
do

  output=testresults-network-pdfbox-networkon-saflateoff-${counter}.zip
  #
  # cd networkon-saflateoff
  mkdir -p "networkon-saflateoff"  && cd "$_"

  docker run --rm -v $PWD:/transfer saflate-network-pdfbox  /bin/bash  -c "mvn -o -fn clean install -l build-summary.log -f pdfbox/pom.xml; mvn surefire-report:report -l build-summary.log -f pdfbox/pom.xml;
  zip -r /transfer/$output pdfbox/pdfbox/target/surefire-reports pdfbox/pdfbox/target/site/surefire-report.html pdfbox/examples/target/surefire-reports pdfbox/examples/target/site/surefire-report.html pdfbox/fontbox/target/surefire-reports pdfbox/fontbox/target/site/surefire-report.html pdfbox/preflight/target/surefire-reports pdfbox/preflight/target/site/surefire-report.html pdfbox/tools/target/surefire-reports pdfbox/tools/target/site/surefire-report.html pdfbox/xmpbox/target/surefire-reports pdfbox/xmpbox/target/site/surefire-report.html build-summary.log;" -v .:/transfer
  #
  cd ..
  output=testresults-network-pdfbox-networkon-saflateon-${counter}.zip

  #cd networkon-saflateon
  mkdir -p "networkon-saflateon"  && cd "$_"

  docker run --rm -v $PWD:/transfer saflate-network-pdfbox  /bin/bash  -c "mvn -o -fn clean install -l build-summary.log -f pdfbox-saflated/pom.xml;mvn surefire-report:report -Djunit.jupiter.extensions.autodetection.enabled=true -Dsaflate.supportconcurrent-test-execution=true -l build-summary.log -f pdfbox-saflated/pom.xml;
  zip -r /transfer/$output pdfbox-saflated/pdfbox/target/surefire-reports pdfbox-saflated/pdfbox/target/site/surefire-report.html pdfbox-saflated/examples/target/surefire-reports pdfbox-saflated/examples/target/site/surefire-report.html pdfbox-saflated/fontbox/target/surefire-reports pdfbox-saflated/fontbox/target/site/surefire-report.html pdfbox-saflated/preflight/target/surefire-reports pdfbox-saflated/preflight/target/site/surefire-report.html pdfbox-saflated/tools/target/surefire-reports pdfbox-saflated/tools/target/site/surefire-report.html pdfbox-saflated/xmpbox/target/surefire-reports pdfbox-saflated/xmpbox/target/site/surefire-report.html build-summary.log;" -v .:/transfer


  cd ..
  output=testresults-network-pdfbox-networkoff-saflateon-${counter}.zip

  #cd networkoff-saflateon
  mkdir -p "networkoff-saflateon"  && cd "$_"

  docker run --rm --network none -v $PWD:/transfer saflate-network-pdfbox  /bin/bash  -c "mvn -o -fn clean install -l build-summary.log -f pdfbox-saflated/pom.xml; mvn test surefire-report:report -Djunit.jupiter.extensions.autodetection.enabled=true -Dsaflate.supportconcurrent-test-execution=true -e -l build-summary.log -f pdfbox-saflated/pom.xml;
  zip -r /transfer/$output pdfbox-saflated/pdfbox/target/surefire-reports pdfbox-saflated/pdfbox/target/site/surefire-report.html pdfbox-saflated/examples/target/surefire-reports pdfbox-saflated/examples/target/site/surefire-report.html pdfbox-saflated/fontbox/target/surefire-reports pdfbox-saflated/fontbox/target/site/surefire-report.html pdfbox-saflated/preflight/target/surefire-reports pdfbox-saflated/preflight/target/site/surefire-report.html pdfbox-saflated/tools/target/surefire-reports pdfbox-saflated/tools/target/site/surefire-report.html pdfbox-saflated/xmpbox/target/surefire-reports pdfbox-saflated/xmpbox/target/site/surefire-report.html build-summary.log;" -v .:/transfer

  cd ..
  output=testresults-network-pdfbox-networkoff-saflateoff-${counter}.zip

  #cd networkoff-saflateoff
  mkdir -p "networkoff-saflateoff"  && cd "$_"

  docker run --rm --network none -v $PWD:/transfer saflate-network-pdfbox  /bin/bash  -c "mvn -o -fn clean install -l build-summary.log -f pdfbox/pom.xml; mvn surefire-report:report -l build-summary.log -f pdfbox/pom.xml;
  zip -r /transfer/$output pdfbox/pdfbox/target/surefire-reports pdfbox/pdfbox/target/site/surefire-report.html pdfbox/examples/target/surefire-reports pdfbox/examples/target/site/surefire-report.html pdfbox/fontbox/target/surefire-reports pdfbox/fontbox/target/site/surefire-report.html pdfbox/preflight/target/surefire-reports pdfbox/preflight/target/site/surefire-report.html pdfbox/tools/target/surefire-reports pdfbox/tools/target/site/surefire-report.html pdfbox/xmpbox/target/surefire-reports pdfbox/xmpbox/target/site/surefire-report.html build-summary.log;" -v .:/transfer

  cd ..


  ((counter++))

done

echo All done
