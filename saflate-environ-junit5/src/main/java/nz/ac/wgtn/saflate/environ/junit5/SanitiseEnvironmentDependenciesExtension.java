package nz.ac.wgtn.saflate.environ.junit5;

import nz.ac.wgtn.ecs.saflate.environ.features.Features;
import nz.ac.wgtn.ecs.saflate.environ.test.RunClassification;
import org.junit.jupiter.api.extension.*;
import org.opentest4j.IncompleteExecutionException;
import org.opentest4j.TestAbortedException;
import java.io.File;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * JUnit rule that detects whether a test is known to fail (failure or error) under certain configurations (= sets of environment variables)
 * that are known. If so, its state is changed to skip.
 * Before using this tests must install a classifier !  When the class is loaded, it will try to auto-install a classifier using two system properties.
 */
public class SanitiseEnvironmentDependenciesExtension implements TestExecutionExceptionHandler  {

    // system variable keys
    public static final String SYSTEM_PROPERTY_CLASSIFIER = "saflate.environ.classifier";
    public static final String SYSTEM_PROPERTY_ARFF = "saflate.environ.arff";

    private static RunClassification CLASSIFIER = null;

    static {
        installFromSystemProperties();
    }

    // try to auto-install from system properties
    public static void installFromSystemProperties() {
        System.out.println("Trying to auto-install classifier for " + SanitiseEnvironmentDependenciesExtension.class.getName() + " from system properties");
        String arffFileName = System.getProperty(SYSTEM_PROPERTY_ARFF);
        String classifierFileName = System.getProperty(SYSTEM_PROPERTY_CLASSIFIER);
        if (arffFileName == null || classifierFileName == null) {
            System.out.println("No properties \'" + SYSTEM_PROPERTY_ARFF + "\' and \'" + SYSTEM_PROPERTY_CLASSIFIER + "\' found to auto-install classifier");
        } else {
            try {
                RunClassification classifier = new RunClassification(new File(classifierFileName), new File(arffFileName));
                CLASSIFIER = classifier;
            } catch (Exception x) {
                System.err.println("Failed to auto-install classifier for " + SanitiseEnvironmentDependenciesExtension.class.getName() + " from system properties");
                x.printStackTrace();
            }
        }
    }

    public static void install(RunClassification classifier) {
        CLASSIFIER = classifier;
    }

    private final static List<String> PROPERTIES = Collections.unmodifiableList(
            Stream.of(Features.ALL)
                .map(feature -> feature.getName())
                .collect(Collectors.toList())
    );

    public static final PropertyProvider SYSTEM_PROPERTY_PROVIDER = new PropertyProvider() {
        @Override
        public String getProperty(String name) {
            return System.getProperty(name);
        }
    };

    public static PropertyProvider PROPERTY_PROVIDER = SYSTEM_PROPERTY_PROVIDER;

    // abstraction to facilitate unit testing with custom providers
    public interface PropertyProvider {
        public String getProperty(String name);
    }

    @Override
    public void handleTestExecutionException(ExtensionContext extensionContext, Throwable throwable) throws Throwable {
        if (CLASSIFIER==null) {
            System.err.println("Test extension " + this.getClass().getName() + " not setup, install a classifier with SanitiseEnvironmentDependenciesExtension.install before use");
            throw throwable;
        }

        boolean testIsErrorOrFailure = !(throwable instanceof IncompleteExecutionException);
        if (testIsErrorOrFailure) {
            Optional<Method> testMethodOpt = extensionContext.getTestMethod();
            if (!testMethodOpt.isPresent()) {
                throw throwable;
            }
            Method testMethod = testMethodOpt.get();
            String testName = testMethod.getDeclaringClass().getName() + '.' + testMethod.getName();

            if (throwable instanceof AssertionError) {
                if (confirm(testName,"failure")) {
                    throw new TestAbortedException("Test is known to fail in this configuration, see classifier rules for details");
                }
            }
            else {
                if (confirm(testName,"error")) {
                    throw new TestAbortedException("Test is known to result in an error in this configuration, see classifier rules for details");
                }
            }
        }
        throw throwable;
    }

    private boolean confirm(String testName, String status) {
        try {
            String[] args = new String[PROPERTIES.size() + 2];
            for (int i = 0; i < PROPERTIES.size(); i++) {
                args[i] = PROPERTY_PROVIDER.getProperty(PROPERTIES.get(i));
            }
            args[PROPERTIES.size()] = testName;
            args[PROPERTIES.size() + 1] = status;
            return CLASSIFIER.confirm(args);
        }
        catch (Exception x) {
            x.printStackTrace();
            return false;
        }

    }


}
