package nz.ac.wgtn.saflate.environ.junit5;

import org.junit.jupiter.api.Assumptions;
import org.junit.platform.engine.TestDescriptor;
import org.junit.platform.engine.TestExecutionResult;
import org.junit.platform.launcher.Launcher;
import org.junit.platform.launcher.LauncherDiscoveryRequest;
import org.junit.platform.launcher.TestExecutionListener;
import org.junit.platform.launcher.TestIdentifier;
import org.junit.platform.launcher.core.LauncherDiscoveryRequestBuilder;
import org.junit.platform.launcher.core.LauncherFactory;
import java.io.File;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.platform.engine.discovery.DiscoverySelectors.selectClass;

public abstract class AbstractTestTests {

    static {
        // set up system properties for classifier auto-install
        File arff = new File(AbstractTestTests.class.getResource("/training.arff").getFile());
        File serClassifier = new File(AbstractTestTests.class.getResource("/classifier.weka").getFile());
        System.setProperty(SanitiseEnvironmentDependenciesExtension.SYSTEM_PROPERTY_CLASSIFIER,serClassifier.getAbsolutePath());
        System.setProperty(SanitiseEnvironmentDependenciesExtension.SYSTEM_PROPERTY_ARFF,arff.getAbsolutePath());

        // this may depend on classloading order -- can be triggered explicitly
        SanitiseEnvironmentDependenciesExtension.installFromSystemProperties();
    }

    private Set<String> testsWithFailedAssumptions = null;
    private Set<String> testsThatFailed = null;
    private int testsExecutedCount = 0;

    // not annotated as fixtures, used in subclass fixtures using super. calls
    public void setup() throws Exception {
        this.testsWithFailedAssumptions = new HashSet<>();
        this.testsThatFailed = new HashSet<>();

        // explicit initialisation, replaced by system properties in static block
        //        File arff = new File(AbstractTestTests.class.getResource("/training.arff").getFile());
        //        File serClassifier = new File(AbstractTestTests.class.getResource("/classifier.weka").getFile());
        //        RunClassification classifier = new RunClassification(serClassifier,arff);
        //        SanitiseEnvironmentDependenciesExtension.install(classifier);
    }
    public void tearDown() {
        this.testsWithFailedAssumptions = null;
        this.testsThatFailed = null;
        this.testsExecutedCount = 0;
    }


    private TestExecutionListener listener = new TestExecutionListener() {

        @Override
        public void executionSkipped(TestIdentifier testIdentifier, String reason) {
            if (testIdentifier.getType()== TestDescriptor.Type.TEST) {
                testsWithFailedAssumptions.add(testIdentifier.getDisplayName());
            }
        }

        @Override
        public void executionStarted(TestIdentifier testIdentifier) {
            TestExecutionListener.super.executionStarted(testIdentifier);
        }

        @Override
        public void executionFinished(TestIdentifier testIdentifier, TestExecutionResult testExecutionResult) {
            if (testIdentifier.getType()== TestDescriptor.Type.TEST) {
                testsExecutedCount = testsExecutedCount + 1;
                if (testExecutionResult.getStatus() == TestExecutionResult.Status.FAILED) {
                    testsThatFailed.add(testIdentifier.getDisplayName());
                }
                else if (testExecutionResult.getStatus() == TestExecutionResult.Status.ABORTED) {
                    testsWithFailedAssumptions.add(testIdentifier.getDisplayName());
                }
            }
        }
    };

    protected void executeTests(Class testClass) {
        LauncherDiscoveryRequest request = LauncherDiscoveryRequestBuilder.request()
                .selectors(selectClass(testClass))
                .build();
        Launcher launcher = LauncherFactory.create();
        launcher.registerTestExecutionListeners(listener);
        launcher.execute(request);
    }

    protected void assumeNumberOfTestsExecuted(int expectedTestCount) {
        Assumptions.assumeTrue(expectedTestCount==this.testsExecutedCount);
    }

    protected void assertFailedTests(String... testNames) {
        Set<String> set = Stream.of(testNames).collect(Collectors.toSet());
        assertEquals(set,testsThatFailed);
    }

    protected void assertSkippedTests(String... testNames) {
        Set<String> set = Stream.of(testNames).collect(Collectors.toSet());
        assertEquals(set,testsWithFailedAssumptions);
    }

}
