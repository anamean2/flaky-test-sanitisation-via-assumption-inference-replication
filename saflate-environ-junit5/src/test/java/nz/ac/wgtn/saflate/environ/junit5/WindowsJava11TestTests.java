package nz.ac.wgtn.saflate.environ.junit5;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class WindowsJava11TestTests extends AbstractTestTests {

    @BeforeEach
    public void setup() throws Exception {
        super.setup();
        SanitiseEnvironmentDependenciesExtension.PROPERTY_PROVIDER = new SanitiseEnvironmentDependenciesExtension.PropertyProvider() {
            @Override
            public String getProperty(String name) {
                if (name.equals("os.name")) {
                    return "Windows";
                }
                else if (name.equals("java.specification.version")) {
                    return "11";
                }
                return SanitiseEnvironmentDependenciesExtension.SYSTEM_PROPERTY_PROVIDER.getProperty(name);
            }
        };
    }

    @AfterEach
    public void tearDown() {
        SanitiseEnvironmentDependenciesExtension.PROPERTY_PROVIDER = SanitiseEnvironmentDependenciesExtension.SYSTEM_PROPERTY_PROVIDER;
        super.tearDown();
    }

    @Test
    public void test() {
        executeTests(org.foo.Test.class);
        assumeNumberOfTestsExecuted(3);
        assertFailedTests();
        assertSkippedTests("test2()","test3()");
    }


}
