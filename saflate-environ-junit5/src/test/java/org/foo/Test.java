package org.foo;

import nz.ac.wgtn.saflate.environ.junit5.SanitiseEnvironmentDependenciesExtension;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Tests used as test data -- implement the scenario used to test the classifier in
 * saflate-environ-junit5
 */
@ExtendWith(SanitiseEnvironmentDependenciesExtension.class)
public class Test {

    @org.junit.jupiter.api.Test
    public void test1() {
        assertTrue(true);
    }

    @org.junit.jupiter.api.Test
    public void test2() {
        // need to reference SanitiseEnvironmentDependenciesExtension here in order to use mock properties to simulate the environment
        // in normal use tests just use System::getProperty
        String value = SanitiseEnvironmentDependenciesExtension.PROPERTY_PROVIDER.getProperty("java.specification.version");
        assertTrue(!value.startsWith("11"));
    }

    @org.junit.jupiter.api.Test
    public void test3() {
        // need to reference SanitiseEnvironmentDependenciesExtension here in order to use mock properties to simulate the environment
        // in normal use tests just use System::getProperty
        String value = SanitiseEnvironmentDependenciesExtension.PROPERTY_PROVIDER.getProperty("os.name");
        if (value.contains("Windows")) {
            throw new IllegalStateException();
        }
        assertTrue(true);
    }


}
