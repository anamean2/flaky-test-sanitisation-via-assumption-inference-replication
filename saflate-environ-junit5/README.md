## Overview

This project defines junit5 extensions to santisise tests known to fail in certain environments / configurations that can be
described in terms of system variables, such as OS kind and version, Java version, JVM vendor etc.

This is achieved by providing an extension that intercepts test execution, looks for tests in an `error` or `failure` state,
then uses a classifier to query whether this was expected in this environment, and if so, switches the state to `skip`.

### Usage

To use the extension, add the following annotation to test classes:

```java
import nz.ac.wgtn.saflate.environ.junit5.SanitiseEnvironmentDependenciesExtension;
...
@ExtendWith(SanitiseEnvironmentDependenciesExtension.class)
```

This artifact is (not yet) in the central Maven repo. To use it, install the project locally by running `mvn install`, then use the following dependency in your project:

```xml
<dependency>
    <groupId>nz.ac.wgtn.ecs.saflate</groupId>
    <artifactId>saflate-environ-core</artifactId>
    <version>0.0.2</version>
    <scope>test</scope>
</dependency>
```

(doublecheck for the correct version number)

This artifact depends on the `saflate-environ-core` module in the same group that also has to be installed using `mvn install`. 

## Auto-Deployment

The extensions can be applied to all tests by adding the following runtime parameter (in an IDE, add this to the run configuration): `-Djunit.jupiter.extensions.autodetection.enabled=true`. 
No further configuration is required, see also https://junit.org/junit5/docs/current/user-guide/#extensions-registration-automatic.  

## Installing the Classifier

The extension needs a classifier to work. A classifier can be built from surefire reports from test runs using a script in the
`saflate-environ-core` module, check the readme in this module for instructions how to produce the respective files.

Such a classifier can be installed using the `SanitiseEnvironmentDependenciesExtension::install` method. A more elegant way to do this 
is to set two system variables (e.g., as JVM arguments) pointing to the files containing the classifier and the training data 
(which contains the feature definitions used by the classifier which are necessary to run classifications). 

The respective variables are:

1. `saflate.environ.classifier` -- the serialised classifier
2. `saflate.environ.arff` -- the training set used to build the classifier (in weka's `arff` format)
