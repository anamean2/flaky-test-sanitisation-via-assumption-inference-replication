package nz.ac.wgtn.ecs.saflate.environ.train;

import org.junit.jupiter.api.Test;
import java.io.File;
import java.io.IOException;
import java.util.List;
import static nz.ac.wgtn.ecs.saflate.environ.train.Utils.resource2file;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;

public class TestSurefireReportParser {

    @Test
    public void testProperties1 () throws IOException {
        File report = resource2file("/surefire-reports/run1/");
        List<TestSuite> testSuites = SurefireReportParser.parse(report);

        assertSame(1,testSuites.size());
        TestSuite testSuite = testSuites.get(0);
        assertSame(18,testSuite.getProperties().size());
        assertEquals("16",testSuite.getProperties().get("java.specification.version"));
        assertEquals("Oracle Corporation",testSuite.getProperties().get("java.vm.vendor"));
        assertEquals("Linux",testSuite.getProperties().get("os.name"));
        assertEquals("16.0.2+7-67",testSuite.getProperties().get("java.runtime.version"));
        assertEquals("16.0.2+7-67",testSuite.getProperties().get("java.vm.version"));
    }

    @Test
    public void testTestCases1 () throws IOException {
        File report = resource2file("/surefire-reports/run1/");
        List<TestSuite> testSuites = SurefireReportParser.parse(report);

        assertSame(1,testSuites.size());
        TestSuite testSuite = testSuites.get(0);
        assertSame(5,testSuite.getTestCases().size());

        assertEquals("org.foo.Test",testSuite.getTestCases().get(0).getClassName());
        assertEquals("test1",testSuite.getTestCases().get(0).getName());
        assertEquals(TestCaseEvaluationResult.success,testSuite.getTestCases().get(0).getStatus());

        assertEquals("org.foo.Test",testSuite.getTestCases().get(1).getClassName());
        assertEquals("test2",testSuite.getTestCases().get(1).getName());
        assertEquals(TestCaseEvaluationResult.success,testSuite.getTestCases().get(1).getStatus());

        assertEquals("org.foo.Test",testSuite.getTestCases().get(2).getClassName());
        assertEquals("test3",testSuite.getTestCases().get(2).getName());
        assertEquals(TestCaseEvaluationResult.success,testSuite.getTestCases().get(2).getStatus());

        assertEquals("org.foo.Test",testSuite.getTestCases().get(3).getClassName());
        assertEquals("test4",testSuite.getTestCases().get(3).getName());
        assertEquals(TestCaseEvaluationResult.failure,testSuite.getTestCases().get(3).getStatus());

        assertEquals("org.foo.Test",testSuite.getTestCases().get(4).getClassName());
        assertEquals("test5",testSuite.getTestCases().get(4).getName());
        assertEquals(TestCaseEvaluationResult.error,testSuite.getTestCases().get(4).getStatus());
    }

    @Test
    public void testProperties2 () throws IOException {
        File report = resource2file("/surefire-reports/run2/");
        List<TestSuite> testSuites = SurefireReportParser.parse(report);

        assertSame(1,testSuites.size());
        TestSuite testSuite = testSuites.get(0);
        assertSame(18,testSuite.getProperties().size());
        assertEquals("11",testSuite.getProperties().get("java.specification.version"));
        assertEquals("Oracle Corporation",testSuite.getProperties().get("java.vm.vendor"));
        assertEquals("Linux",testSuite.getProperties().get("os.name"));
        assertEquals("11.0.2+9-LTS",testSuite.getProperties().get("java.runtime.version"));
        assertEquals("11.0.2+9",testSuite.getProperties().get("java.vm.version"));
    }

    @Test
    public void testTestCases2 () throws IOException {
        File report = resource2file("/surefire-reports/run2/");
        List<TestSuite> testSuites = SurefireReportParser.parse(report);

        assertSame(1,testSuites.size());
        TestSuite testSuite = testSuites.get(0);
        assertSame(5,testSuite.getTestCases().size());

        assertEquals("org.foo.Test",testSuite.getTestCases().get(0).getClassName());
        assertEquals("test1",testSuite.getTestCases().get(0).getName());
        assertEquals(TestCaseEvaluationResult.success,testSuite.getTestCases().get(0).getStatus());

        assertEquals("org.foo.Test",testSuite.getTestCases().get(1).getClassName());
        assertEquals("test2",testSuite.getTestCases().get(1).getName());
        assertEquals(TestCaseEvaluationResult.failure,testSuite.getTestCases().get(1).getStatus());

        assertEquals("org.foo.Test",testSuite.getTestCases().get(2).getClassName());
        assertEquals("test3",testSuite.getTestCases().get(2).getName());
        assertEquals(TestCaseEvaluationResult.success,testSuite.getTestCases().get(2).getStatus());

        assertEquals("org.foo.Test",testSuite.getTestCases().get(3).getClassName());
        assertEquals("test4",testSuite.getTestCases().get(3).getName());
        assertEquals(TestCaseEvaluationResult.failure,testSuite.getTestCases().get(3).getStatus());

        assertEquals("org.foo.Test",testSuite.getTestCases().get(4).getClassName());
        assertEquals("test5",testSuite.getTestCases().get(4).getName());
        assertEquals(TestCaseEvaluationResult.error,testSuite.getTestCases().get(4).getStatus());
    }

    @Test
    public void testProperties3 () throws IOException {
        File report = resource2file("/surefire-reports/run3/");
        List<TestSuite> testSuites = SurefireReportParser.parse(report);

        assertSame(1,testSuites.size());
        TestSuite testSuite = testSuites.get(0);
        assertSame(18,testSuite.getProperties().size());
        assertEquals("16",testSuite.getProperties().get("java.specification.version"));
        assertEquals("Oracle Corporation",testSuite.getProperties().get("java.vm.vendor"));
        assertEquals("Windows 10",testSuite.getProperties().get("os.name"));
        assertEquals("16.0.2+7-67",testSuite.getProperties().get("java.runtime.version"));
        assertEquals("16.0.2+7-67",testSuite.getProperties().get("java.vm.version"));
    }

    @Test
    public void testTestCases3 () throws IOException {
        File report = resource2file("/surefire-reports/run3/");
        List<TestSuite> testSuites = SurefireReportParser.parse(report);

        assertSame(1,testSuites.size());
        TestSuite testSuite = testSuites.get(0);
        assertSame(5,testSuite.getTestCases().size());

        assertEquals("org.foo.Test",testSuite.getTestCases().get(0).getClassName());
        assertEquals("test1",testSuite.getTestCases().get(0).getName());
        assertEquals(TestCaseEvaluationResult.success,testSuite.getTestCases().get(0).getStatus());

        assertEquals("org.foo.Test",testSuite.getTestCases().get(1).getClassName());
        assertEquals("test2",testSuite.getTestCases().get(1).getName());
        assertEquals(TestCaseEvaluationResult.success,testSuite.getTestCases().get(1).getStatus());

        assertEquals("org.foo.Test",testSuite.getTestCases().get(2).getClassName());
        assertEquals("test3",testSuite.getTestCases().get(2).getName());
        assertEquals(TestCaseEvaluationResult.error,testSuite.getTestCases().get(2).getStatus());

        assertEquals("org.foo.Test",testSuite.getTestCases().get(3).getClassName());
        assertEquals("test4",testSuite.getTestCases().get(3).getName());
        assertEquals(TestCaseEvaluationResult.failure,testSuite.getTestCases().get(3).getStatus());

        assertEquals("org.foo.Test",testSuite.getTestCases().get(4).getClassName());
        assertEquals("test5",testSuite.getTestCases().get(4).getName());
        assertEquals(TestCaseEvaluationResult.error,testSuite.getTestCases().get(4).getStatus());
    }
}
