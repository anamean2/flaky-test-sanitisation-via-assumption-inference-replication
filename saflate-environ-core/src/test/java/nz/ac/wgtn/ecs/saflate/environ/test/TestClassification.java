package nz.ac.wgtn.ecs.saflate.environ.test;

import com.google.common.base.Preconditions;
import nz.ac.wgtn.ecs.saflate.environ.test.RunClassification;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import weka.classifiers.Classifier;
import weka.core.*;

import java.io.*;

import static nz.ac.wgtn.ecs.saflate.environ.train.Utils.resource2file;
import static org.junit.jupiter.api.Assertions.*;

public class TestClassification {

    private RunClassification classy = null;

    @BeforeEach
    public void setup() throws Exception {
        Classifier classifier = null;
        File file = resource2file("/classifier.weka");
        Preconditions.checkArgument(file.exists());
        try (InputStream in = new FileInputStream(file)) {
            classifier = (Classifier) SerializationHelper.read(in);
        }
        catch (Exception x) {
            x.printStackTrace();
        }

        Instances trainingSet = null;
        file = resource2file("/training.arff");
        Preconditions.checkArgument(file.exists());
        try (FileReader in = new FileReader(file)) {
            trainingSet = new Instances(in);
        }
        catch (Exception x) {
            x.printStackTrace();
        }
        this.classy = new RunClassification(classifier,trainingSet);
    }

    @AfterEach
    public void tearDown() {
        this.classy = null;
    }

    @Test
    public void testRun1Test1Expected() throws Exception {
        // this instance is in the training set
        // '16','16','Oracle','amd64','Linux','org.foo.Test.test1','success'
        boolean result = this.classy.confirm("16","16","Oracle","Linux","org.foo.Test.test1","success");
        assertTrue(result);
    }

    @Test
    public void testRun1Test1Unexpected1() throws Exception {
        boolean result = this.classy.confirm("16","16","Oracle","Linux","org.foo.Test.test1","failure");
        assertFalse(result);
    }

    @Test
    public void testRun1Test1Unexpected2() throws Exception {
        boolean result = classy.confirm("16","16","Oracle","Linux","org.foo.Test.test1","error");
        assertFalse(result);
    }

    @Test
    public void testRun1Test2Expected() throws Exception {
        // this instance is in the training set
        // '16','16','Oracle','amd64','Linux','org.foo.Test.test2','success'
        boolean result = classy.confirm("16","16","Oracle","Linux","org.foo.Test.test2","success");
        assertTrue(result);
    }

    @Test
    public void testRun1Test2Unexpected2() throws Exception {
        boolean result = classy.confirm("16","16","Oracle","Linux","org.foo.Test.test2","error");
        assertFalse(result);
    }

    @Test
    public void testRun1Test2Unexpected3() throws Exception {
        boolean result = classy.confirm("16","16","Oracle","Linux","org.foo.Test.test2","failure");
        assertFalse(result);
    }

    @Test
    public void testRun1Test3Expected() throws Exception {
        // this instance is in the training set
        // '16','16','Oracle','amd64','Linux','org.foo.Test.test3','success'
        boolean result = classy.confirm("16","16","Oracle","Linux","org.foo.Test.test3","success");
        assertTrue(result);
    }

    @Test
    public void testRun1Test3Unexpected1() throws Exception {
        boolean result = classy.confirm("16","16","Oracle","Linux","org.foo.Test.test3","error");
        assertFalse(result);
    }

    @Test
    public void testRun1Test3Unexpected2() throws Exception {
        boolean result = classy.confirm("16","16","Oracle","Linux","org.foo.Test.test3","error");
        assertFalse(result);
    }

    @Test
    public void testRun2Test1Expected() throws Exception {
        // this instance is in the training set
        // '16','16','Oracle','amd64','Linux','org.foo.Test.test1','success'
        boolean result = classy.confirm("16","16","Oracle","Linux","org.foo.Test.test1","success");
        assertTrue(result);
    }

    @Test
    public void testRun2Test1Unexpected1() throws Exception {
        boolean result = classy.confirm("16","16","Oracle","Linux","org.foo.Test.test1","failure");
        assertFalse(result);
    }

    @Test
    public void testRun2Test1Unexpected2() throws Exception {
        boolean result = classy.confirm("16","16","Oracle","Linux","org.foo.Test.test1","error");
        assertFalse(result);
    }

    @Test
    public void testRun2Test2Expected() throws Exception {
        boolean result = classy.confirm("11","11","Oracle","Linux","org.foo.Test.test2","failure");
        assertTrue(result);
    }

    @Test
    public void testRun2Test2Unexpected1() throws Exception {
        boolean result = classy.confirm("11","11","Oracle","Linux","org.foo.Test.test2","success");
        assertFalse(result);
    }

    @Test
    public void testRun2Test2Unexpected2() throws Exception {
        boolean result = classy.confirm("11","11","Oracle","Linux","org.foo.Test.test2","error");
        assertFalse(result);
    }

    @Test
    public void testRun2Test3Expected() throws Exception {
        boolean result = classy.confirm("11","11","Oracle","Linux","org.foo.Test.test3","success");
        assertTrue(result);
    }

    @Test
    public void testRun2Test3Unexpected1() throws Exception {
        boolean result = classy.confirm("11","11","Oracle","Linux","org.foo.Test.test3","error");
        assertFalse(result);
    }

    @Test
    public void testRun2Test3Unexpected2() throws Exception {
        boolean result = classy.confirm("11","11","Oracle","Linux","org.foo.Test.test3","failure");
        assertFalse(result);
    }

    @Test
    public void testRun3Test1Expected() throws Exception {
        // this instance is in the training set
        // '16','16','Oracle','amd64','Linux','org.foo.Test.test1','success'
        boolean result = classy.confirm("16","16","Oracle","Windows","org.foo.Test.test1","success");
        assertTrue(result);
    }

    @Test
    public void testRun3Test1Unexpected1() throws Exception {
        boolean result = classy.confirm("16","16","Oracle","Windows","org.foo.Test.test1","failure");
        assertFalse(result);
    }

    @Test
    public void testRun3Test1Unexpected2() throws Exception {
        boolean result = classy.confirm("16","16","Oracle","Windows","org.foo.Test.test1","error");
        assertFalse(result);
    }

    @Test
    public void testRun3Test2Expected() throws Exception {
        // this instance is in the training set
        // '16','16','Oracle','amd64','Linux','org.foo.Test.test2','success'
        boolean result = classy.confirm("16","16","Oracle","Windows","org.foo.Test.test2","success");
        assertTrue(result);
    }

    @Test
    public void testRun3Test2Unexpected1() throws Exception {
        boolean result = classy.confirm("16","16","Oracle","Windows","org.foo.Test.test2","error");
        assertFalse(result);
    }

    @Test
    public void testRun3Test2Unexpected2() throws Exception {
        boolean result = classy.confirm("16","16","Oracle","Windows","org.foo.Test.test2","failure");
        assertFalse(result);
    }

    @Test
    public void testRun3Test3Expected() throws Exception {
        boolean result = classy.confirm("16","16","Oracle","Windows","org.foo.Test.test3","error");
        assertTrue(result);
    }

    @Test
    public void testRun3Test3Unexpected1() throws Exception {
        boolean result = classy.confirm("16","16","Oracle","Windows","org.foo.Test.test3","success");
        assertFalse(result);
    }

    @Test
    public void testRun3Test3Unexpected2() throws Exception {
        boolean result = classy.confirm("16","16","Oracle","Windows","org.foo.Test.test3","failure");
        assertFalse(result);
    }

    // input validation testing
    @Test
    public void testNotEnoughParameters() {
        assertThrows(IllegalArgumentException.class,() -> {
            classy.confirm("16","16","Oracle","Windows","org.foo.Test.test3");
        });
    }

    @Test
    public void testTooManyParameters() {
        assertThrows(IllegalArgumentException.class,() -> {
            classy.confirm("16","16","Oracle","Windows","org.foo.Test.test3","failure","foo");
        });
    }

    @Test
    public void testTestNotInTrainingSet() {
        assertThrows(IllegalArgumentException.class,() -> {
            classy.confirm("16","16","Oracle","Windows","org.foo.Test.nonExistingTest","failure");
        });
    }

}
