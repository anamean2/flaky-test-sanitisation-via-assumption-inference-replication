package nz.ac.wgtn.ecs.saflate.environ.train;

import java.io.File;

public class Utils {

    public static File resource2file(String resourceName) {
        // resource names start with '/'
        return new File(TestSurefireReportParser.class.getResource(resourceName).getFile());
    }

    public static String resource2filename(String resourceName) {
        return resource2file(resourceName).getAbsolutePath();
    }
}
