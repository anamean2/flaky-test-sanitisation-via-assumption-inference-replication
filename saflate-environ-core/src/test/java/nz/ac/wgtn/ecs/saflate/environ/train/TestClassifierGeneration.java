package nz.ac.wgtn.ecs.saflate.environ.train;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static nz.ac.wgtn.ecs.saflate.environ.train.Utils.resource2filename;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TestClassifierGeneration {

    static final String OUTPUT_FOLDER = "tmp/";

    static String[] SUREFIRE_REPORT_FOLDERS = new String[] {
        resource2filename("/surefire-reports/run1"),
        resource2filename("/surefire-reports/run2"),
        resource2filename("/surefire-reports/run3"),
        resource2filename("/surefire-reports/run4"),
        resource2filename("/surefire-reports/run5"),
        resource2filename("/surefire-reports/run6")
    };

    static String getSurefireFoldersArgValue() {
        return Stream.of(SUREFIRE_REPORT_FOLDERS).collect(Collectors.joining(","));
    }

    @BeforeAll
    public static void setup() {
        new File(OUTPUT_FOLDER).mkdirs();
    }

    @Test
    public void testPreconditionEnforcement1 () {
        // not enough runs to build classifier
        assertThrows(IllegalStateException.class, () -> {
            Main.main(new String[]{
                "-"+Main.SUREFIRE_REPORTS,SUREFIRE_REPORT_FOLDERS[0],
                "-"+Main.OUT_CLASSIFIER,OUTPUT_FOLDER+"classifier.weka",
                "-"+Main.OUT_ARFF,OUTPUT_FOLDER+"training.arff",
                "-"+Main.OUT_RULES,OUTPUT_FOLDER+"provenance.txt"
            });
        });
    }

    @Test
    public void testClassifierCreation1 () {
        // not enough runs to build classifier
        assertDoesNotThrow(() -> {
            Main.main(new String[]{
                "-"+Main.SUREFIRE_REPORTS,getSurefireFoldersArgValue(),
                "-"+Main.OUT_CLASSIFIER,OUTPUT_FOLDER+"classifier.weka",
                "-"+Main.OUT_ARFF,OUTPUT_FOLDER+"training.arff",
                "-"+Main.OUT_RULES,OUTPUT_FOLDER+"provenance.txt"
            });
        });
    }
}
