## scenario 1

the scenario contains 3 surefire reports for three fictional tests (`test1`, `test2`, `test3`) with the following simple pattern failure pattern:

| run | os      | java version  | test1    | test2    | test3     | test4     | test5     |
| --- | ------- | ------------- | -------- | -------- | --------- | -------- | ---------  |
| 1   | Linux   | 16            | success  | success  | success   | failure  | error      |
| 2   | Linux   | 11            | success  | failure  | success   | failure  | error      |
| 3   | Windows | 16            | success  | success  | error     | failure  | error      |
| 4   | Windows | 11            | success  | failure  | error     | failure  | error      |
| 5   | Mac OS X| 16            | success  | success  | success   | failure  | error      |
| 6   | Mac OS X| 11            | success  | failure  | success   | failure  | error      |

