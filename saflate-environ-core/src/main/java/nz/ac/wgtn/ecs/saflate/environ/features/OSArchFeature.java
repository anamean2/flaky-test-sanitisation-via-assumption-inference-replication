package nz.ac.wgtn.ecs.saflate.environ.features;

import java.util.stream.Collectors;
import java.util.stream.Stream;

public class OSArchFeature extends EnumFeature {
    public enum OSArch {aarch64,amd64,x86_64,x86_32,amd32,unknown};

    @Override
    protected String name() {
        return "os.arch";
    }

    @Override
    public String getType() {
        return Stream.of(OSArch.values())
            .map(v -> "\'" + v.name() + '\'')
            .collect(Collectors.joining(",","{","}"));

    }

    @Override
    public String convert(String value) {
        try {
            return OSArchFeature.OSArch.valueOf(value).name();
        }
        catch (IllegalArgumentException x) {
            return OSArchFeature.OSArch.unknown.name();
        }
    }
}
