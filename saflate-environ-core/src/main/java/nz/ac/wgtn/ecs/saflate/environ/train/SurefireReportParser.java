package nz.ac.wgtn.ecs.saflate.environ.train;

import com.google.common.base.Preconditions;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 * Parses test reports from xml surefire reports complying to https://maven.apache.org/surefire/maven-surefire-plugin/xsd/surefire-test-report.xsd .
 */
public class SurefireReportParser {
    public static List<TestSuite> parse(File folder) throws IOException {
        List<TestSuite> testSuites = new ArrayList<>();
        getTestReports(folder).forEach(f -> {
            try {
                parse(f,testSuites);
            }
            catch (IOException x) {
                Main.LOGGER.error("Error parsing test report " + f,x);
            }
        });
        return testSuites;
    }

    private static Stream<Path> getTestReports(File folder) throws IOException {
        Preconditions.checkState(folder!=null,"folder cannot be null");
        Preconditions.checkState(folder.exists(),"folder does not exist: " + folder.toPath().toString());
        Preconditions.checkState(folder.isDirectory(),"this is not a folder: " + folder.toPath().toString());
        return Files.walk(folder.toPath())
            .filter(Files::isRegularFile)
            .filter(f -> f.toFile().getName().startsWith("TEST-"))
            .filter(f -> f.toFile().getName().endsWith(".xml"));
    }

    static boolean hasReports(File folder) {
        try {
            return getTestReports(folder).findAny().isPresent();
        }
        catch (IOException x) {
            Main.LOGGER.error("Error checking " + folder.toPath().toString() + " for surefire test reports");
            return false;
        }
    }

    static void parse(Path testReport, List<TestSuite> testSuites) throws IOException {

        try {
            TestSuite testSuite = new TestSuite();
            testSuite.setReport(testReport);
            Document doc = readXml(testReport.toFile());
            Element root = doc.getDocumentElement();
            assert root.getTagName().equals("testsuite");
            NodeList nodes = root.getElementsByTagName("properties");
            assert nodes.getLength() == 1;
	        if (nodes.getLength() == 0) {
	            // FIXME: case in pentaho-kettle where xml does not comply with expected format
                Main.LOGGER.error("Error parsing " + testReport.toString() + " - will skip");
                return;
            }
            Element eProperties = (Element) nodes.item(0);
            nodes = eProperties.getElementsByTagName("property");
            for (int i = 0; i < nodes.getLength(); i++) {
                Element eProperty = (Element) nodes.item(i);
                String name = eProperty.getAttribute("name");
                String value = eProperty.getAttribute("value");
                testSuite.setProperty(name, value);
            }
            nodes = root.getElementsByTagName("testcase");
            for (int i = 0; i < nodes.getLength(); i++) {
                Element eTestcase = (Element) nodes.item(i);
                String name = eTestcase.getAttribute("name");
                String className = eTestcase.getAttribute("classname");
                TestCaseEvaluationResult status = null;
                // failure,rerunFailure,flakyFailure,skipped,error,rerunError,flakyError
                if (eTestcase.getElementsByTagName("failure").getLength() > 0) {
                    status = TestCaseEvaluationResult.failure;
                } else if (eTestcase.getElementsByTagName("error").getLength() > 0) {
                    status = TestCaseEvaluationResult.error;
                } else if (eTestcase.getElementsByTagName("rerunFailure").getLength() > 0) {
                    Main.LOGGER.warn("ignoring test state rerunFailure");
                } else if (eTestcase.getElementsByTagName("flakyFailure").getLength() > 0) {
                    Main.LOGGER.warn("ignoring test state flakyFailure");
                } else if (eTestcase.getElementsByTagName("rerunError").getLength() > 0) {
                    Main.LOGGER.warn("ignoring test state rerunError");
                } else if (eTestcase.getElementsByTagName("flakyError").getLength() > 0) {
                    Main.LOGGER.warn("ignoring test state flakyError");
                } else if (eTestcase.getElementsByTagName("skipped").getLength() > 0) {
                    Main.LOGGER.warn("ignoring test state skipped");
                } else {
                    status = TestCaseEvaluationResult.success;
                }

                if (status == null) {
                    Main.LOGGER.warn("Ignoring " + className + "::" + name);
                } else {
                    TestCase testCase = new TestCase();
                    testCase.setClassName(className);
                    testCase.setName(name);
                    testCase.setStatus(status);
                    testSuite.addTestCase(testCase);
                }
            }

            testSuites.add(testSuite);

        } catch (SAXException | ParserConfigurationException | IOException x) {
            Main.LOGGER.error("Error parsing " + testReport.toString() + " - will skip", x);
        }

    }

    private static Document readXml(File f) throws IOException, SAXException, ParserConfigurationException {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        dbFactory.setValidating(false);
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        dBuilder.setEntityResolver(new EntityResolver() {
            @Override
            public InputSource resolveEntity(String publicId, String systemId)
                    throws SAXException, IOException {
                if (systemId.contains("report.dtd")) {
                    return new InputSource(new FileReader("report.dtd"));
                } else {
                    return null;
                }
            }
        });
        Document doc = dBuilder.parse(f);
        doc.getDocumentElement().normalize();
        return doc;
    }

}
