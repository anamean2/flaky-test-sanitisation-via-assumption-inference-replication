package nz.ac.wgtn.ecs.saflate.environ.train;

import com.google.common.base.Preconditions;
import com.google.common.io.Files;
import nz.ac.wgtn.ecs.saflate.environ.features.Feature;
import nz.ac.wgtn.ecs.saflate.environ.features.Features;
import nz.ac.wgtn.ecs.saflate.environ.features.TestNameFeatureBuilder;
import nz.ac.wgtn.ecs.saflate.environ.features.TestStatusFeature;
import org.apache.commons.cli.*;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.logging.log4j.core.config.DefaultConfiguration;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.rules.JRip;
import weka.core.Instances;
import weka.core.SerializationHelper;
import java.io.*;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

/**
 * Executable to build a classifier.
 * Based on JRip.
 */
public class Main {
    public static final String EXCLUDE_REPORTS = "excludeReports"; // for testing
    public static final String SUREFIRE_REPORTS = "surefirereports";
    public static final String OUT_CLASSIFIER = "classifier";
    public static final String OUT_ARFF = "arff";
    public static final String OUT_RULES = "provenance";
    public static final String OUT_EVALUATION = "evaluation";
    public static final String FOLDS = "folds";

    public static final Logger LOGGER = LogManager.getLogger("build-test-classifier");

    public static void main (String[] args) throws ParseException {

        long startTime = System.currentTimeMillis();

        Options options = new Options();
        options.addOption(SUREFIRE_REPORTS, true, "a comma-separated list of folders with surefire test reports (TEST-<testclassname>.xml) (required)");
        options.addOption(OUT_CLASSIFIER, true, "the file used to save the classifier (required)");
        options.addOption(OUT_ARFF, true, "the file used to save the classifier schema in weka arff format (required)");
        options.addOption(OUT_RULES, true, "the file used to save the generated classification rules in human readable format (optional)");
        options.addOption(OUT_EVALUATION, true, "the file used to save the cross-validation evaluation summary");
        options.addOption(FOLDS, true, "the number of folds for cross-validation");

        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = parser.parse( options, args);

        if (!(cmd.hasOption(SUREFIRE_REPORTS) && cmd.hasOption(OUT_CLASSIFIER)) && cmd.hasOption(OUT_ARFF)) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("java -cp <classpath> " + Main.class.getName(), "", options, "", true);
            System.exit(1);
        }

        // initialise logging

        Configurator.initialize(new DefaultConfiguration());
        Configurator.setRootLevel(Level.INFO);

        LOGGER.info("loading features");
        List<Feature> features = Stream.of(Features.ALL).collect(toList());
        Preconditions.checkState(features.size()>0,"no features found");
        LOGGER.info("" + features.size() + " features loaded");

        LOGGER.info("loading surefire test reports");
        String value = cmd.getOptionValue(SUREFIRE_REPORTS);
        String[] reports = value.split(",");
        Preconditions.checkState(reports.length>1,"more then one report folder is required");

        List<File> foldersWithReports = Stream.of(reports)
            .map(name -> new File(name))
            .filter(folder -> {
                if (!SurefireReportParser.hasReports(folder)) {
                    LOGGER.warn("no surefire reports found in " + folder.toPath());
                    return false;
                }
                else {
                    return true;
                }
            })
            .collect(toList());

        Preconditions.checkArgument(foldersWithReports.size()>1,"At least two folders with surefire reports are required to build a classifier");

        // parse reports
        List<List<TestSuite>> runs = new ArrayList<>();
        for (File folder:foldersWithReports) {
            try {
                List<TestSuite> suites = SurefireReportParser.parse(folder);
                runs.add(suites);
            }
            catch (IOException x) {
                Main.LOGGER.error("Error parsing test report in " + folder.toPath(),x);
            }
        }

        // check reports against features -- make sure that all properties are present
        LOGGER.info("validating reports against features");
        runs.stream().forEach(suites -> checkTestSuitesAgainstFeatures(suites,features));

        // remove tests which do not succeed at least once -- those are not considered as flaky
        LOGGER.info("removing tests from training set which do not succeed at least once -- those are not considered flaky");
        List<TestSuite> suites = runs.stream().flatMap(s -> s.stream()).collect(toList());
        removeNeverSucceedingTests(suites);
        LOGGER.info("removing flaky test cases with success status in the same environment");

        // if flaky same run (i.e. same vendor,version,os) then remove succeeding results
        removeClassNoise(suites);

        // build arff as input for weka
        LOGGER.info("formatting training data for weka (arff format)");
        String arff = buildArff(features,runs);

        if (cmd.hasOption(OUT_ARFF)) {
            String arffFileName = cmd.getOptionValue(OUT_ARFF);
            LOGGER.info("exporting weka training data to " + arffFileName);
            File arffFile = new File(arffFileName);
            try {
                Files.write(arff, arffFile, Charset.defaultCharset());
                LOGGER.info("weka training data exported to " + arffFile.getAbsolutePath());
            }
            catch (IOException x) {
                LOGGER.error("error exporting weka training data to " + arffFile.getAbsolutePath(),x);
            }
        }

        try {
            Instances trainingSet = new Instances(new StringReader(arff)) ;
            trainingSet.setClassIndex(features.size()+1); // two columns added, we want to predict status
            JRip classifier = new JRip();
            classifier.buildClassifier(trainingSet);
            LOGGER.info("classifier successfully built");

            String outputFile = cmd.getOptionValue(OUT_CLASSIFIER);
            try (OutputStream out = new FileOutputStream(outputFile)) {
                SerializationHelper.write(out, classifier);
                LOGGER.info("classifier saved to " + outputFile);

                long endTime = System.currentTimeMillis();
                LOGGER.info("Building classifier took " + (endTime-startTime) + "ms");
            }
            catch (IOException x) {
                LOGGER.error("error exporting classifier",x);
            }

            if (cmd.hasOption(OUT_RULES)) {
                outputFile = cmd.getOptionValue(OUT_RULES);
                try (PrintWriter out = new PrintWriter(new FileWriter(outputFile))) {
                    // TODO improve on this -- may require some digging in the internals of JRip
                    out.println(classifier.toString());
                    LOGGER.info("human-readable rules used to " + outputFile);
                }
                catch (IOException x) {
                    LOGGER.error("error exporting classifier",x);
                }
            }


            if (cmd.hasOption(OUT_EVALUATION) && cmd.hasOption(FOLDS)) {
                String evaluationFileName = cmd.getOptionValue(OUT_EVALUATION);
                int folds = 10;
                try {
                    folds = Integer.parseInt(cmd.getOptionValue(FOLDS));
                }
                catch (NumberFormatException x) {
                    folds = 10;

                }
                LOGGER.info("starting evaluation");

                try (PrintWriter out = new PrintWriter(new FileWriter(evaluationFileName))) {
                    out.println(getEvaluationSummary(trainingSet, folds));
                    LOGGER.info("evaluation written to: {}", evaluationFileName);
                }
                catch (IOException x) {
                    LOGGER.error("error exporting evaluation summary",x);
                }
            }


        } catch (IOException x) {
            LOGGER.error("error reading weka training data from memory source",x);
        } catch (Exception x) {
            LOGGER.error("error building classifier",x);
        }

    }

    private static void removeClassNoise(List<TestSuite> testSuites) {

        for(TestSuite ts1: testSuites) {
            for(TestSuite ts2: testSuites) {
                if (ts1 == ts2)
                    continue;
                if (ts1.getProperties().getProperty("os.name").equals(ts2.getProperties().getProperty("os.name")) &&
                    ts1.getProperties().getProperty("java.vm.vendor").equals(ts2.getProperties().getProperty("java.vm.vendor")) &&
                    ts1.getProperties().getProperty("java.specification.version").equals(ts2.getProperties().getProperty("java.specification.version")) &&
                    ts1.getProperties().getProperty("java.vm.specification.version").equals(ts2.getProperties().getProperty("java.vm.specification.version"))) {
                    List<TestCase> toRemove = new ArrayList<>();
                    for(TestCase tc1: ts1.getTestCases()) {
                        Iterator<TestCase> itr = ts2.getTestCases().iterator();
                        while(itr.hasNext()) {
                            TestCase tc2 = itr.next();
                            if (tc1.getClassName().equals(tc2.getClassName()) && tc1.getName().equals(tc2.getName())) {
                                if (tc1.getStatus() != tc2.getStatus()) {
                                    if (tc2.getStatus() == TestCaseEvaluationResult.success) {
                                        toRemove.add(tc2);
                                        LOGGER.info("Removing same env flaky test: {}", tc2);
                                    }
                                }
                            }
                        }
                    }
                    ts2.getTestCases().removeAll(toRemove);
                }
            }
        }
    }

    private static void removeNeverSucceedingTests(List<TestSuite> testSuites) {
        final Function<TestCase,String> testCase2string = testCase -> testCase.getClassName() + "::" + testCase.getName();
        Set<String> sometimesSucceedingTestNames = testSuites.stream()
            .flatMap(suite -> suite.getTestCases().stream())
            .filter(testCase -> testCase.getStatus()==TestCaseEvaluationResult.success)
            .map(testCase2string) // must omit status here !
            .collect(Collectors.toSet());

        for (TestSuite testSuite:testSuites) {
            Iterator<TestCase> iter = testSuite.getTestCases().iterator();
            while (iter.hasNext()) {
                TestCase testCase = iter.next();
                String testCaseName = testCase2string.apply(testCase);
                if (!sometimesSucceedingTestNames.contains(testCaseName)) {
                    iter.remove();
                    LOGGER.info("Removed test " + testCase.toString() + " from suite -- it never succeeds and is not considered flaky");
                }
            }
        }
    }

    // build the weka input file in memory
    private static String buildArff(List<Feature> features, List<List<TestSuite>> runs) {
        StringWriter sWriter = new StringWriter();
        PrintWriter out = new PrintWriter(sWriter);

        Set<TestSuite> allTestSuites = runs.stream()
            .flatMap(list -> list.stream())
            .collect(Collectors.toSet());

        // header
        out.println("% flaky test classification");
        out.println("% generator: " + Main.class.getName());
        out.println("% timestamp: " + DateFormat.getDateInstance().format(new Date()));
        out.println();

        // schema
        out.println("@RELATION flaky");
        for (Feature feature:features) {
            out.println("@ATTRIBUTE \'" + feature.getName() + "\' " + feature.getType());
        }
        // add features to identify test and test outcome
        Feature testNameFeature = new TestNameFeatureBuilder().apply(allTestSuites);
        out.println("@ATTRIBUTE \'" + testNameFeature.getName() + "\' " + testNameFeature.getType());
        Feature testStateFeature = new TestStatusFeature();
        out.println("@ATTRIBUTE \'" + testStateFeature.getName() + "\' " + testStateFeature.getType());
        out.println();

        // data
        out.println("@DATA");
        Set<List<String>> alreadySeen = new HashSet<>();
        for (List<TestSuite> suites:runs) {
            for (TestSuite suite:suites) {
                String standardFeatureData = features.stream()
                    // guaranteed to exist from prechecks
                    .map(feature -> feature.convert(suite.getProperties().getProperty(feature.getName())))
                    .map(v -> "\'"+v+'\'')
                    .collect(Collectors.joining(",","",","));
                // append test specific data
                for (TestCase testcase:suite.getTestCases()) {
                    List<String> testData = List.of(standardFeatureData, testcase.getClassName(), testcase.getName(), testcase.getStatus().name());
                    if (alreadySeen.contains(testData))
                        continue;
                    alreadySeen.add(testData);
                    out.print(standardFeatureData);
                    out.print('\'');
                    out.print(testcase.getClassName());
                    out.print('.');
                    out.print(testcase.getName());
                    out.print("\',\'");
                    out.print(testcase.getStatus().name());
                    out.println('\'');
                }
            }
        }

        out.println();
        out.close();
        return sWriter.toString();
    }

    private static void checkTestSuitesAgainstFeatures(List<TestSuite> suites, List<Feature> features) {
        for (TestSuite suite:suites) {
            for (Feature feature:features) {
                checkTestSuiteAgainstFeature(suite, feature);
            }
        }
    }

    private static void checkTestSuiteAgainstFeature(TestSuite suite, Feature feature) {
        Preconditions.checkArgument(suite.getProperties().containsKey(feature.getName()),"test report " + suite.getReport() + " does not contain feature property " + feature.getName());
    }

    private static String getEvaluationSummary(Instances instances, int folds) throws Exception {
        Random rand = new Random();
        long seed = rand.nextLong();
        rand.setSeed(seed);
        Instances randData = instances;
        randData.randomize(rand);
        // perform cross-validation
        randData.stratify(folds);
        Evaluation eval = new Evaluation(randData);
        for (int n = 0; n < folds; n++) {
            Instances train = randData.trainCV(folds, n, rand);
            Instances test = randData.testCV(folds, n);
            // build and evaluate classifier
            Classifier clsCopy = new JRip();
            clsCopy.buildClassifier(train);
            eval.evaluateModel(clsCopy, test);
        }

        String output = "";
        output = output + "Folds: " + folds + "\n";
        output = output + "Seed: " + seed + "\n";
        output = output + eval.toSummaryString("=== " + folds + "-fold Cross-validation ===", false);
	    output = output + eval.toMatrixString();

        return output;
    }

}
