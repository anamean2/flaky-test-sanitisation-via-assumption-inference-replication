package nz.ac.wgtn.ecs.saflate.environ.test;

import com.google.common.base.Preconditions;
import nz.ac.wgtn.ecs.saflate.environ.features.Features;
import nz.ac.wgtn.ecs.saflate.environ.features.Feature;
import nz.ac.wgtn.ecs.saflate.environ.features.TestNameFeature;
import nz.ac.wgtn.ecs.saflate.environ.features.TestStatusFeature;
import weka.classifiers.Classifier;
import weka.core.*;
import java.io.*;
import java.util.*;

/**
 * API to run classifications using the generated classifier.
 */
public class RunClassification {

    private Classifier classifier = null;
    private Instances instances = null;
    private ArrayList<Attribute> attributes = null; // weka requires ArrayList as input !
    private Set<String> testNames = new HashSet<>();
    private List<Feature> features = new ArrayList<>();

    /**
     * @throws IllegalArgumentException if checks on the attributes defined in the schema (of the training set) (instances) fail
     */
    public RunClassification(Classifier classifier, Instances instances) {
        init(classifier,instances);
    }

    /**
     * Convenient constructor.
     * @param serialisedClassifier -- the serialised classifier
     * @param trainingSet -- the training set in arff format
     * @throws IllegalArgumentException if checks on the attributes defined in the schema (of the training set) (instances) fail
     * @throws Exception I/O or deserialisation-related exceptions
     */
    public RunClassification (File serialisedClassifier,File trainingSet) throws Exception {
        Preconditions.checkArgument(serialisedClassifier.exists());
        Preconditions.checkArgument(trainingSet.exists());

        Classifier classifier1 = null;
        try (FileInputStream in = new FileInputStream(serialisedClassifier)) {
            classifier1 = (Classifier) SerializationHelper.read(in);
        }

        Instances instances1 = null;
        try (Reader in = new InputStreamReader(new FileInputStream(trainingSet))) {
            instances1 = new Instances(in);
        }

        init(classifier1,instances1);
    }

    private void init(Classifier classifier, Instances instances) {
        this.classifier = classifier;
        this.instances = instances;

        // the reason we get this from the training set is the type of the testname feature -- this is an enum
        // of all testnames encountered when the classifier was built
        ArrayList<Attribute> attrs = new ArrayList<>();
        for (int i=0;i<instances.numAttributes();i++) {
            attrs.add(instances.attribute(i));
        }

        Preconditions.checkArgument(attrs.size()>2,"unexpected number of atttributes");
        Preconditions.checkArgument(attrs.get(attrs.size()-2).name().equals(TestNameFeature.FEATURE_NAME),"unexpected second to last attribute, should be " + TestNameFeature.FEATURE_NAME);
        Preconditions.checkArgument(attrs.get(attrs.size()-1).name().equals(TestStatusFeature.FEATURE_NAME),"unexpected last attribute, should be " + TestStatusFeature.FEATURE_NAME);
        this.attributes = attrs;

        // extract test names
        Attribute testNameAttribute = this.attributes.get(attrs.size()-2);
        Enumeration<Object> valueIter = testNameAttribute.enumerateValues();
        while (valueIter.hasMoreElements()) {
            testNames.add((String)valueIter.nextElement());
        }

        // install features
        List<Feature> ftrs = new ArrayList<>();
        for (Feature feature:Features.ALL) {
            ftrs.add(feature);
        }
        ftrs.add(new TestNameFeature(testNames));
        ftrs.add(new TestStatusFeature());

        Preconditions.checkArgument(ftrs.size()==attributes.size(),"unexpected number of features, does not match schema (training set) that was used to build the classifier");
        for (int i=0;i<attributes.size();i++) {
            Preconditions.checkArgument(attributes.get(i).name().equals(ftrs.get(i).getName()),"unexpected feature at position " + i);
        }
        this.features = ftrs;
    }

    /**
     * Test a record. The params have to comply to the (datatypes of the) features that were used to build the classifier.
     * The last parameter is the one that is being classified, the method will return true if the classiers reaches the same
     * result, and false otherwise.
     * @param params
     * @throws IllegalArgumentException if the number of parameters does not match the number of features used to build the classifier, or if the datatype does not match
     * this is in particular the case of a testname parameter is used that is not part of the training set
     * @return true if the classiers reaches the same result as indicated in the last parameter, and false otherwise.
     */
    public boolean confirm(String... params) throws Exception {

        // validate parameters
        Preconditions.checkArgument(params.length==features.size(),"unexpected number of parameters, does not match number of parameters that was used to build the classifier");

        String[] sanitisedParams = new String[params.length];
        for (int i=0;i<this.attributes.size();i++) {
            Feature feature = this.features.get(i);
            String param = params[i];
            // note: some validation is happening here, and may result in further IllegalArgumentExceptions
            sanitisedParams[i] = feature.convert(param);
        }

        // build test set for classification
        Instances testdata = new Instances("TestInstances", attributes, 0);
        Instance instance = new DenseInstance(attributes.size());
        testdata.add(instance);
        instance.setDataset(testdata);
        testdata.setClassIndex(testdata.numAttributes() - 1);

        for (int i=0;i<sanitisedParams.length;i++) {
            Attribute attr = this.attributes.get(i);
            if (attr.isNumeric()) {
                int v = Integer.parseInt(sanitisedParams[i]);
                instance.setValue(i,v);
            }
            else {
                instance.setValue(i, sanitisedParams[i]);
            }
        }

        double result = classifier.classifyInstance(instance);

        // use junit approach to deal with some edge cases
        // https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/lang/Double.html#equals(java.lang.Object)
        return  Double.doubleToLongBits(result) == Double.doubleToLongBits(instance.classValue());
    }
}
