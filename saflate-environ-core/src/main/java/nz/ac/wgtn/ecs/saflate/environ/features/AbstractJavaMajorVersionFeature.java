package nz.ac.wgtn.ecs.saflate.environ.features;

public abstract class AbstractJavaMajorVersionFeature extends IntFeature  {

    @Override
    public String convert(String value) {
        if (isInt(value)) {
            return value;
        }
        else if (value.startsWith("1.1")) {
            return "1";
        }
        else if (value.startsWith("1.2")) {
            return "2";
        }
        else if (value.startsWith("1.3")) {
            return "3";
        }
        else if (value.startsWith("1.4")) {
            return "4";
        }
        else if (value.startsWith("1.5")) {
            return "5";
        }
        else if (value.startsWith("1.6")) {
            return "6";
        }
        else if (value.startsWith("1.7")) {
            return "7";
        }
        else if (value.startsWith("1.8")) {
            return "8";
        }
        else if (value.startsWith("1.9")) {
            return "9";
        }
        else {
            String[] tokens = value.split(".");
            if (tokens.length>0) {
                if (isInt(tokens[0])) {
                    return tokens[0];
                }
            }
        }
        return "-1"; // unknown

    }

    private boolean isInt(String value) {
        try {
            Integer.parseInt(value);
            return true;
        }
        catch (NumberFormatException x) {
            return false;
        }

    }
}