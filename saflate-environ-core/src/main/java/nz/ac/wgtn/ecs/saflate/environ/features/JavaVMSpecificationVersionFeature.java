package nz.ac.wgtn.ecs.saflate.environ.features;

public class JavaVMSpecificationVersionFeature extends AbstractJavaMajorVersionFeature {
    @Override
    protected String name() {
        return "java.vm.specification.version";
    }
}
