package nz.ac.wgtn.ecs.saflate.environ.features;

import nz.ac.wgtn.ecs.saflate.environ.features.*;

/**
 * Fixed registry for features.
 * This excludes two features: testname (created dynamically based on the tests in scope), and teststatus (used for classification)
 */
public class Features {

    public static final Feature[] ALL = {
        new JavaSpecificationVersionFeature(),
        new JavaVMSpecificationVersionFeature(),
        new JavaVMVendorFeature(),
        new OSNameFeature()
    };
}
