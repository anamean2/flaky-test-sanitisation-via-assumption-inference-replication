package nz.ac.wgtn.ecs.saflate.environ.features;

/**
 * Abstract class for numeric features.
 */
public abstract class IntFeature extends Feature {

    @Override
    public String getType() {
        return "numeric";
    }
}
