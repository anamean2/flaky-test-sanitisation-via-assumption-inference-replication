package nz.ac.wgtn.ecs.saflate.environ.train;

/**
 * Represents the result of evaluating a test.
 * Surefires own flakiness handling is not supported.
 * Part of a simplified domain model defined by https://maven.apache.org/surefire/maven-surefire-plugin/xsd/surefire-test-report.xsd .
 */
public enum TestCaseEvaluationResult {
    failure,error,success
}
