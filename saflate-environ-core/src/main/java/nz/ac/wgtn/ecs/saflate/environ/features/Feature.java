package nz.ac.wgtn.ecs.saflate.environ.features;

import java.util.Objects;

/**
 * Feature (in the weka sense) corresponding to a system property.
 */
public abstract class Feature {

    // the name of the Java system property recorded in test reports (surefire)
    private String name = name();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    protected abstract String name();

    public abstract String convert(String value);

    // the weka type definition
    // see https://waikato.github.io/weka-wiki/formats_and_processing/arff_stable/
    public abstract String getType();

    @Override
    public String toString() {
        return "Feature{" +
                "name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Feature feature = (Feature) o;
        return Objects.equals(name, feature.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }


}
