package nz.ac.wgtn.ecs.saflate.environ.features;

import java.util.stream.Collectors;
import java.util.stream.Stream;

public class JavaVMVendorFeature extends EnumFeature {
    public enum JavaVMVendor {Oracle,Eclipse,IBM,unknown};

    @Override
    protected String name() {
        return "java.vm.vendor";
    }

    @Override
    public String getType() {
        return Stream.of(JavaVMVendor.values())
            .map(v -> "\'" + v.name() + '\'')
            .collect(Collectors.joining(",","{","}"));
    }

    @Override
    public String convert(String value) {
        try {
            if (value.contains("Oracle Corporation")) {
                return JavaVMVendor.Oracle.name();
            }
            else if (value.contains("IBM Corporation")) {
                return JavaVMVendor.IBM.name();
            }
            else if (value.contains("OpenJ9")) {
                return JavaVMVendor.IBM.name();
            }
            else if (value.contains("Eclipse Foundation")) {
                return JavaVMVendor.Eclipse.name();
            }
            else if (value.contains("Temurin")) {
                return JavaVMVendor.Eclipse.name();
            }
            else {
                return JavaVMVendor.unknown.name();
            }
        }
        catch (IllegalArgumentException x) {
            return JavaVMVendor.unknown.name();
        }
    }
}
