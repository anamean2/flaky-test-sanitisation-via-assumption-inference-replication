package nz.ac.wgtn.ecs.saflate.environ.features;

public class JavaSpecificationVersionFeature extends AbstractJavaMajorVersionFeature {
    @Override
    protected String name() {
        return "java.specification.version";
    }
}
