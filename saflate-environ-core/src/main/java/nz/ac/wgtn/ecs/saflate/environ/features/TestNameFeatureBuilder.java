package nz.ac.wgtn.ecs.saflate.environ.features;

import nz.ac.wgtn.ecs.saflate.environ.train.TestSuite;
import java.util.Collection;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * The test name feature is dynamically generated for a set of tests, this is to build an enum type
 * and to avoid a string type.
 */
public class TestNameFeatureBuilder implements Function<Collection<TestSuite>,Feature> {


    @Override
    public Feature apply(Collection<TestSuite> testSuites) {

        final Set<String> testNames = testSuites.stream()
            .flatMap(suite -> suite.getTestCases().stream())
            .map(testcase -> "\'" + testcase.getClassName() + '.' + testcase.getName() + '\'')
            .collect(Collectors.toSet());

        return new TestNameFeature(testNames);

    }
}
