package nz.ac.wgtn.ecs.saflate.environ.features;

import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TestStatusFeature extends EnumFeature {
    public enum TestStatus {failure,success,error,unknown};

    public static final String FEATURE_NAME = "teststatus";

    @Override
    protected String name() {
        return FEATURE_NAME;
    }

    @Override
    public String getType() {
        return Stream.of(TestStatus.values())
            .map(v -> "\'" + v.name() + '\'')
            .collect(Collectors.joining(",","{","}"));
    }

    @Override
    public String convert(String value) {
        try {
            return TestStatusFeature.TestStatus.valueOf(value).name();
        }
        catch (IllegalArgumentException x) {
            return TestStatus.unknown.name();
        }
    }
}
