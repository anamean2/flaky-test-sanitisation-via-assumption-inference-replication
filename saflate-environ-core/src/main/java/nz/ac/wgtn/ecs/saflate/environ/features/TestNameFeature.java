package nz.ac.wgtn.ecs.saflate.environ.features;

import com.google.common.base.Preconditions;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Test name feature.
 */
public class TestNameFeature extends EnumFeature {

    public static final String FEATURE_NAME = "testname";

    private Set<String> testNames = null;
    private String type = null;

    public TestNameFeature(Set<String> testNames) {
        Preconditions.checkArgument(testNames.size()>0);
        this.testNames = testNames;
        this.type = testNames.stream().collect(Collectors.joining(",","{","}"));
    }

    @Override
    protected String name() {
        return FEATURE_NAME;
    }

    @Override
    public String convert(String value) {
        Preconditions.checkArgument(testNames.contains(value));
        return value;
    }

    @Override
    public String getType() {
        return type;
    }
}
