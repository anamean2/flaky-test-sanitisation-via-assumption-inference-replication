package nz.ac.wgtn.ecs.saflate.environ.features;

import java.util.stream.Collectors;
import java.util.stream.Stream;

public class OSNameFeature extends EnumFeature {
    public enum OSName {Linux,Windows,MacOSX,unknown};

    @Override
    protected String name() {
        return "os.name";
    }

    @Override
    public String getType() {
        return Stream.of(OSName.values())
            .map(v -> "\'" + v.name() + '\'')
            .collect(Collectors.joining(",","{","}"));
    }

    @Override
    public String convert(String value) {
        try {
            if (value.contains("Linux")) {
                return OSName.Linux.name();
            }
            else if (value.contains("Windows")) {
                return OSName.Windows.name();
            }
            else if (value.contains("Mac OS X")) {
                return OSName.MacOSX.name();
            }
            else {
                return OSName.unknown.name();
            }
        }
        catch (IllegalArgumentException x) {
            return OSName.unknown.name();
        }
    }
}
