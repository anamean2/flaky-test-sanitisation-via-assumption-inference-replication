#!/bin/sh

# build PDFBox image
docker build -f Dockerfile-network-pdfbox -t saflate-network-pdfbox .

# build jsoup image
docker build -f Dockerfile-network-jsoup -t saflate-network-jsoup .

# build Jabref image
docker build -f Dockerfile-network-jabref -t saflate-network-jabref .

#build jmeter image
docker build -f Dockerfile-network-jmeter -t saflate-network-jmeter .

#build biojava image
docker build -f Dockerfile-network-biojava -t saflate-network-biojava .


#build swagger-parser image
docker build -f Dockerfile-network-swagger-parser -t saflate-network-swagger-parser .
