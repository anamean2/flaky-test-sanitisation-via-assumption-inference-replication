package nz.ac.wgtn.saflate.environ.junit4;

import net.bytebuddy.ByteBuddy;
import net.bytebuddy.agent.ByteBuddyAgent;
import net.bytebuddy.asm.Advice;
import net.bytebuddy.dynamic.loading.ClassReloadingStrategy;
import nz.ac.wgtn.ecs.saflate.environ.features.Features;
import nz.ac.wgtn.ecs.saflate.environ.test.RunClassification;
import org.junit.AssumptionViolatedException;
import org.junit.internal.runners.statements.InvokeMethod;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.Statement;


import java.io.File;
import java.lang.instrument.Instrumentation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static net.bytebuddy.matcher.ElementMatchers.named;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsInstanceOf.instanceOf;

/**
 * JUnit rule that detects whether a test is known to fail (failure or error) under certain configurations (= sets of environment variables)
 * that are known. If so, its state is changed to skip.
 * Before using this tests must install a classifier !  When the class is loaded, it will try to auto-install a classifier using two system properties.
 */
public class SanitiseEnvironmentDependenciesRule implements TestRule {
    ClassReloadingStrategy classReloadingStrategy = ClassReloadingStrategy.fromInstalledAgent(ClassReloadingStrategy.Strategy.REDEFINITION);

    static {
        // instrument test statement class
        assertThat(ByteBuddyAgent.install(), instanceOf(Instrumentation.class));
        Class clazz = InvokeMethod.class;
        ClassReloadingStrategy classReloadingStrategy = ClassReloadingStrategy.fromInstalledAgent(ClassReloadingStrategy.Strategy.REDEFINITION);
        try {
            new ByteBuddy()
                    .redefine(clazz)
                    .visit(Advice.to(SanitiseEnvironmentDependenciesRule.InvokeMethodAdvice.class).on(named("evaluate")))
                    .make()
                    .load(SanitiseEnvironmentDependenciesRule.InvokeMethodAdvice.class.getClassLoader(), classReloadingStrategy);
        }
        catch (Exception x) {
            x.printStackTrace();
        }

    }

    static class InvokeMethodAdvice {

        public static final String SYSTEM_PROPERTY_FAILED_DEPENDENCIES = "register.failedInferredDependencies";

        @Advice.OnMethodEnter
        public static void onEnter(@Advice.This InvokeMethod self) throws Exception {
            Field testMethodField = InvokeMethod.class.getDeclaredField("testMethod");
            testMethodField.setAccessible(true);
            FrameworkMethod ftest = (FrameworkMethod) testMethodField.get(self);
            Method testMethod = ftest.getMethod();
            String testName = testMethod.getDeclaringClass().getName() + '.' + testMethod.getName();
            String predictedResult = "";

            // check whether rule is active
            StackTraceElement[] frames = new Throwable().getStackTrace();
            for (StackTraceElement frame : frames) {
                if (frame.getClassName().equals(SanitiseEnvironmentDependenciesRule.
                        InferredEnvironDependency2AssumptionViolatedRuleStatement.class.getName())) {
			try {
                    if (confirm(testName, "failure"))
                        predictedResult = "failure";
                    if (confirm(testName, "error"))
                        predictedResult = "error";
			} catch (Exception e) {
			 System.err.println("error classifying" + testName);
			}

                    System.setProperty(SYSTEM_PROPERTY_FAILED_DEPENDENCIES, predictedResult);
                }
            }


        }

    }

    // system variable keys
    public static final String SYSTEM_PROPERTY_CLASSIFIER = "saflate.environ.classifier";
    public static final String SYSTEM_PROPERTY_ARFF = "saflate.environ.arff";

    private static RunClassification CLASSIFIER = null;

    static {
        installFromSystemProperties();
    }

    // try to auto-install from system properties
    public static void installFromSystemProperties() {
        System.out.println("Trying to auto-install classifier for " + SanitiseEnvironmentDependenciesRule.class.getName() + " from system properties");
        String arffFileName = System.getProperty(SYSTEM_PROPERTY_ARFF);
        String classifierFileName = System.getProperty(SYSTEM_PROPERTY_CLASSIFIER);
        if (arffFileName == null || classifierFileName == null) {
            System.out.println("No properties \'" + SYSTEM_PROPERTY_ARFF + "\' and \'" + SYSTEM_PROPERTY_CLASSIFIER + "\' found to auto-install classifier");
        } else {
            try {
                RunClassification classifier = new RunClassification(new File(classifierFileName), new File(arffFileName));
                CLASSIFIER = classifier;
            } catch (Exception x) {
                System.err.println("Failed to auto-install classifier for " + SanitiseEnvironmentDependenciesRule.class.getName() + " from system properties");
                x.printStackTrace();
            }
        }
    }

    public static void install(RunClassification classifier) {
        CLASSIFIER = classifier;
    }

    private final static List<String> PROPERTIES = Collections.unmodifiableList(
            Stream.of(Features.ALL)
                .map(feature -> feature.getName())
                .collect(Collectors.toList())
    );

    public static final PropertyProvider SYSTEM_PROPERTY_PROVIDER = new PropertyProvider() {
        @Override
        public String getProperty(String name) {
            return System.getProperty(name);
        }
    };

    public static PropertyProvider PROPERTY_PROVIDER = SYSTEM_PROPERTY_PROVIDER;

    @Override
    public Statement apply(Statement statement, Description description) {
        return new SanitiseEnvironmentDependenciesRule.InferredEnvironDependency2AssumptionViolatedRuleStatement(statement);
    }

    // abstraction to facilitate unit testing with custom providers
    public interface PropertyProvider {
        public String getProperty(String name);
    }



    public static boolean confirm(String testName, String status) {
        try {
            String[] args = new String[PROPERTIES.size() + 2];
            for (int i = 0; i < PROPERTIES.size(); i++) {
                args[i] = PROPERTY_PROVIDER.getProperty(PROPERTIES.get(i));
            }
            args[PROPERTIES.size()] = testName;
            args[PROPERTIES.size() + 1] = status;
            return CLASSIFIER.confirm(args);
        }
        catch (Exception x) {
            x.printStackTrace();
            return false;
        }

    }


    public static class InferredEnvironDependency2AssumptionViolatedRuleStatement extends Statement {

        private Statement next;
        public InferredEnvironDependency2AssumptionViolatedRuleStatement(Statement base) {
            this.next = base;
        }

        @Override
        public void evaluate() throws Throwable {
            String predictedClass = "";
            try {
                predictedClass = System.getProperty(InvokeMethodAdvice.SYSTEM_PROPERTY_FAILED_DEPENDENCIES);
                next.evaluate();
                // still reset -- perhaps exception has occured but test succeeded
                System.setProperty(InvokeMethodAdvice.SYSTEM_PROPERTY_FAILED_DEPENDENCIES, "");
            } catch (Throwable throwable) {
                predictedClass = System.getProperty(InvokeMethodAdvice.SYSTEM_PROPERTY_FAILED_DEPENDENCIES);
                if (predictedClass == null || predictedClass.equals(""))
                    throw throwable;
                if (predictedClass.equals("failure")) {
                    // reset
                    System.setProperty(InvokeMethodAdvice.SYSTEM_PROPERTY_FAILED_DEPENDENCIES, "");
                    throw new AssumptionViolatedException("Test is known to fail in this configuration, see classifier rules for details");
                }
                if (predictedClass.equals("error")) {
                    // reset
                    System.setProperty(InvokeMethodAdvice.SYSTEM_PROPERTY_FAILED_DEPENDENCIES, "");
                    throw new AssumptionViolatedException("Test is known to result in an error in this configuration, see classifier rules for details");
                }

            }
        }
    }



}
