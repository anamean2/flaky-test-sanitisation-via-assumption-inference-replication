package nz.ac.wgtn.saflate.environ.junit4;

import org.junit.Assume;
import org.junit.runner.Description;
import org.junit.runner.JUnitCore;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunListener;

import java.util.HashSet;
import java.util.Set;
import static org.junit.Assert.*;

import java.io.File;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public abstract class AbstractTestTests {

    static {
        // set up system properties for classifier auto-install
        File arff = new File(AbstractTestTests.class.getResource("/training.arff").getFile());
        File serClassifier = new File(AbstractTestTests.class.getResource("/classifier.weka").getFile());
        System.setProperty(SanitiseEnvironmentDependenciesRule.SYSTEM_PROPERTY_CLASSIFIER,serClassifier.getAbsolutePath());
        System.setProperty(SanitiseEnvironmentDependenciesRule.SYSTEM_PROPERTY_ARFF,arff.getAbsolutePath());

        // this may depend on classloading order -- can be triggered explicitly
        SanitiseEnvironmentDependenciesRule.installFromSystemProperties();
    }

    private Set<String> testsWithFailedAssumptions = null;
    private Set<String> testsThatFailed = null;
    private int testsExecutedCount = 0;

    // not annotated as fixtures, used in subclass fixtures using super. calls
    public void setup() throws Exception {
        this.testsWithFailedAssumptions = new HashSet<>();
        this.testsThatFailed = new HashSet<>();

        // explicit initialisation, replaced by system properties in static block
        //        File arff = new File(AbstractTestTests.class.getResource("/training.arff").getFile());
        //        File serClassifier = new File(AbstractTestTests.class.getResource("/classifier.weka").getFile());
        //        RunClassification classifier = new RunClassification(serClassifier,arff);
        //        nz.ac.wgtn.saflate.environ.junit4.SanitiseEnvironmentDependenciesExtension.install(classifier);
    }
    public void tearDown() {
        this.testsWithFailedAssumptions = null;
        this.testsThatFailed = null;
        this.testsExecutedCount = 0;
    }


    private RunListener listener = new RunListener() {
        @Override
        public void testAssumptionFailure(Failure failure) {
            super.testAssumptionFailure(failure);
            testsWithFailedAssumptions.add(failure.getDescription().getMethodName());
        }

        @Override
        public void testStarted(Description description) throws Exception {
            super.testStarted(description);
            testsExecutedCount++;
        }

        @Override
        public void testFailure(Failure failure) throws Exception {
            super.testFailure(failure);
            System.err.println(failure.getMessage());
            System.err.println(failure.getTrace());
            System.err.println(failure);
            testsThatFailed.add(failure.getDescription().getMethodName());
        }

    };

    protected void executeTests(Class testClass) {
        JUnitCore junit = new JUnitCore();
        junit.addListener(listener);
        junit.run(testClass);
    }

    protected void assumeNumberOfTestsExecuted(int expectedTestCount) {
        Assume.assumeTrue(expectedTestCount==this.testsExecutedCount);
    }

    protected void assertFailedTests(String... testNames) {
        Set<String> set = Stream.of(testNames).collect(Collectors.toSet());
        assertEquals(set,testsThatFailed);
    }

    protected void assertSkippedTests(String... testNames) {
        Set<String> set = Stream.of(testNames).collect(Collectors.toSet());
        assertEquals(set,testsWithFailedAssumptions);
    }

}
