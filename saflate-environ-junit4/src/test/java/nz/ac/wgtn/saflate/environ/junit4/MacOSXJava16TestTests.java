package nz.ac.wgtn.saflate.environ.junit4;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class MacOSXJava16TestTests extends AbstractTestTests {

    @Before
    public void setup() throws Exception {
        super.setup();
        SanitiseEnvironmentDependenciesRule.PROPERTY_PROVIDER = new SanitiseEnvironmentDependenciesRule.PropertyProvider() {
            @Override
            public String getProperty(String name) {
                if (name.equals("os.name")) {
                    return "Mac OS X";
                }
                else if (name.equals("java.specification.version")) {
                    return "16";
                }
                return SanitiseEnvironmentDependenciesRule.SYSTEM_PROPERTY_PROVIDER.getProperty(name);
            }
        };
    }

    @After
    public void tearDown() {
        SanitiseEnvironmentDependenciesRule.PROPERTY_PROVIDER = SanitiseEnvironmentDependenciesRule.SYSTEM_PROPERTY_PROVIDER;
        super.tearDown();
    }

    @Test
    public void test() {
        executeTests(org.foo.Test.class);
        assumeNumberOfTestsExecuted(3);
        assertFailedTests();
        assertSkippedTests();
    }


}
