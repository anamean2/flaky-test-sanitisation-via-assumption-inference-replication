package org.foo;

import nz.ac.wgtn.saflate.environ.junit4.SanitiseEnvironmentDependenciesRule;
import org.junit.Rule;
import org.junit.rules.TestRule;


import static junit.framework.TestCase.assertTrue;


/**
 * Tests used as test data -- implement the scenario used to test the classifier in
 * saflate-environ-nz.ac.wgtn.saflate.environ.junit4.junit5
 */
public class Test {
    @Rule
    public TestRule rule = new SanitiseEnvironmentDependenciesRule();

    @org.junit.Test
    public void test1() {
        assertTrue(true);
    }

    @org.junit.Test
    public void test2() {
        // need to reference nz.ac.wgtn.saflate.environ.junit4.SanitiseEnvironmentDependenciesExtension here in order to use mock properties to simulate the environment
        // in normal use tests just use System::getProperty
        String value = SanitiseEnvironmentDependenciesRule.PROPERTY_PROVIDER.getProperty("java.specification.version");
        assertTrue(!value.startsWith("11"));
    }

    @org.junit.Test
    public void test3() {
        // need to reference nz.ac.wgtn.saflate.environ.junit4.SanitiseEnvironmentDependenciesExtension here in order to use mock properties to simulate the environment
        // in normal use tests just use System::getProperty
        String value = SanitiseEnvironmentDependenciesRule.PROPERTY_PROVIDER.getProperty("os.name");
        if (value.contains("Windows")) {
            throw new IllegalStateException();
        }
        assertTrue(true);
    }


}
