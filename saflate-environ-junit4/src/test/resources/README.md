## overview of tests used in tests 

| run | os      | java version  | test1    | test2    | test3     |
| --- | ------- | ------------- | -------- | -------- | --------- |
| 1   | Linux   | 16            | success  | success  | success   |
| 2   | Linux   | 11            | success  | failure  | success   |
| 3   | Windows | 16            | success  | success  | error     |
| 4   | Windows | 11            | success  | failure  | error     |
| 5   | Mac OS X| 16            | success  | success  | success   |
| 6   | Mac OS X| 11            | success  | failure  | success   |

