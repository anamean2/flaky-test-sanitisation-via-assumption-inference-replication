## Overview 

This repo defines junit4 rules to santisise tests that are flaky due to environment dependencies. This is achieved by intercepting test execution, and 
programmatically enforcing assumptions that may lead to flakiness. The respective tests will be flagged as ignored when executed. 

## SanitiseEnvironmentDependencies

The rule is `SanitiseEnvironmentDependenciesRule`.

This rules intercepts exceptions that indicate that tests may fail due to environment dependency. Flakiness occurs as tests that would otherwise succeed 
result in failure or error when executed in a different environment (e.g., jdk version/vendor, OS). 

### Usage

To use the rule, add the following field to test classes:

```java
@Rule public SanitiseEnvironmentDependenciesRule rule = new SanitiseEnvironmentDependenciesRule();
```

This artifact is (not yet) in the central Maven repo. To use it, install the project locally by running `mvn install`, then use the following dependency in your project:

```xml
<dependency>
    <groupId>nz.ac.wgtn.ecs.saflate</groupId>
    <artifactId>saflate-environ-junit4</artifactId>
    <version>0.0.1</version>
    <scope>test</scope>
</dependency>
```

(doublecheck for the correct version number)
